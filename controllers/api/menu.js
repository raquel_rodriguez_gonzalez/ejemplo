// --- VARIABLES GLOBALES ----
var router = require("express").Router();
var Menu = require("../../models/menu");
var Imatge = require("../../models/imagenes");
var path = require('path');


var compressor = path.resolve(__dirname, '../../compressor.js');  // Fitxer que manipules les imatges
function compressAndResize (imageUrl) {
  // Creem un "child process" d'aquesta manera no 
  // fem un bloqueig del EventLoop amb un ús intens de la CPU
  // al processar les imatges
  var childProcess = require('child_process').fork(compressor);
  childProcess.on('message', function(message) {
    console.log(message);
  });
  childProcess.on('error', function(error) {   // Si el procés rep un missatge l'escriurà a la consola
    console.error(error.stack);
  });
  childProcess.on('exit', function() {  //Quan el procés rep l'event exit mostra un missatge a la consola
    console.log('process exited');
  });
  childProcess.send(imageUrl);
}

// CRUD (SQL) = EXPRESS
// CREATE (INSERT) = POST
// READ (SELECT) = GET
// UPDATE (UPDATE) = PUT
// DELETE (DELETE) = DELETE
//------------ GET ---------------------
router.get("/", function(req,res, next){
	Menu.find(function(err,menus){
		if(err) {
			return next(err);
		}
		res.json(menus);
	});
});

//------ GET --------- /id/:id -----------------------
router.get("/_id/:id", function(req,res,next){
    console.log(req.params.id);
    Menu.find({"_id": req.params.id}).find(function(err, menu) {
        if (err) {
            return next(err);
        }
        res.json(menu);
    });
});

//------------------ POST SENCILLO APP JAVAFX ----------------------
router.post("/javafx", function (req,res, next) {
    if (req.auth){
        var menu = new Menu({
            nombre : req.body.nombre,
            primeros : req.body.primeros,
            segundos : req.body.segundos,
            precio : req.body.precio
        });
        menu.save(function(err, menu){
            if(err) {return next(err)}
            res.status(201).json(menu);
        });        
     } else {
         res.status(401).json({"missatge":"No autoritzat"});
     }

});
router.post("/", function (req,res, next) {
     var imatge = new Imatge({
        originalName: req.files.foto.originalname,
        name : req.files.foto.name,
        extension: req.files.foto.extension,
        titol : "",
        descripcion : ""
    });
    
    imatge.save(function(err,imatge) {
        if (err) return next(err);
        compressAndResize(__dirname+"/../../uploads/" + req.files.foto.name);
            var menu = new Menu({
            nombre : req.body.nombre,
            primeros : req.body.primeros,
            segundos : req.body.segundos,
            precio : req.body.precio,
            imagen : imatge._id // Imagen es el Id del Input en el HTML
    	});
        	menu.save(function(err, menu){
        		if(err) {return next(err)}
        		res.status(201).json(menu);
        	});
    });
	
});


//------ DELETE --------- /:id -----------------------
router.delete("/:id", function(req, res, next) {
    Menu.remove({"_id": req.params.id}, function(err) {
        if (err) {
            return next(err);
        }
        res.json({"missatge": "Menu amb id " + req.params.id + " esborrat"});
    });
});

// ---------------- PUT ---------
router.put("/", function(req, res,next) {
    console.log(req.body._id);
    Menu.findByIdAndUpdate(req.body._id,req.body,function(err, menu) {
        if(err) {
            return next(err);
        }
        res.json({"missatge": "Menu modificat"});
    });
});
module.exports = router;