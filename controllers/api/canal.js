module.exports = function(http) {
    var io = require("socket.io")(http);
    io.on('connection', function(socket) {
    });
    return {
        nuevoLibro: function (libro) {
            io.emit('NuevoLibro');
        },
        editarLibro: function () {
            io.emit('EditarLibro');
        },
        eliminarLibro:function () {
            io.emit('EliminarLibro');
        }
    };
};