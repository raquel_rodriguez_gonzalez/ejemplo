// --- VARIABLES GLOBALES ----
var router = require("express").Router();
var Disponibilitat = require("../../models/disponibilitat");

// CRUD (SQL) = EXPRESS
// CREATE (INSERT) = POST
// READ (SELECT) = GET
// UPDATE (UPDATE) = PUT
// DELETE (DELETE) = DELETE
//------------ GET ---------------------
router.get("/", function(req,res, next){
	Disponibilitat.find(function(err,disponibilitats){
		if(err) {
			return next(err);
		}
		res.json(disponibilitats);
	});
});


router.get("/disponibles", function(req,res, next){
	Disponibilitat.find(function(err,disponibilitats){
		if(err) {
			return next(err);
		}
		var diasDisponibles = [];
		var turnosDisponibles = [];
		disponibilitats.forEach(function(e,i){
		    var nuevo = new Date(Date.parse(e.dia));
                    var p = 0;
                    e.turno.forEach(function(a,b){
                        p = p + a.plazas;
                        if (a.plazas == 0){
                        } else {
                            turnosDisponibles.push(a);
                        }
                    });
                    if (p != 0){
                        diasDisponibles.push(e);
                    }
		});
		res.json(diasDisponibles);
	});
});

//------ GET --------- /id/:id -----------------------
router.get("/_id/:id", function(req,res,next){
    console.log(req.params.id);
    Disponibilitat.find({"_id": req.params.id}).find(function(err, disponibilitat) {
        if (err) {
            return next(err);
        }
        res.json(disponibilitat);
    });
});

//------------------ POST ----------------------
router.post("/", function (req,res, next) {
        console.log(req.body);
        var disponibilitat = new Disponibilitat(req.body);
        disponibilitat.save(function(err, disponibilitat){
            if(err) {return next(err)}
            res.status(201).json(disponibilitat);
        });
});


//------ DELETE --------- /:id -----------------------
router.delete("/:id", function(req, res, next) {
    Disponibilitat.remove({"_id": req.params.id}, function(err) {
        if (err) {
            return next(err);
        }
        res.json({"missatge": "Disponibilitat amb id " + req.params.id + " esborrat"});
    });
});

// ---------------- PUT ---------
router.put("/", function(req, res,next) {
    console.log("id Dia: "+req.body._id);
    console.log("id Turno: "+req.body.turno_id);
    console.log("id Plazas: "+req.body.plazas);
    console.log("Accion: "+req.body.accion);
    Disponibilitat.findById(req.body._id, function (err, disponibilitat) {
        if (err) return next(err);
  
        disponibilitat.turno.forEach(function(e,i){
            if (e._id == req.body.turno_id){
                console.log("ID DEL TURNO "+e.nombre + " - " + e._id);
                console.log("plazas: "+e.plazas);
                if (req.body.accion == "mas") {
                    console.log("MAS DIAS");
                    e.plazas = e.plazas + req.body.plazas;
                } else {
                    
                    e.plazas = e.plazas - req.body.plazas;
                }
                console.log("plazas: "+e.plazas);
            }
        });
        disponibilitat.save(function (err) {
            if (err) return next(err);
            res.json(disponibilitat);
        });
    });
        //res.json(disponibilitat);
});

module.exports = router;