// --- VARIABLES GLOBALES ----
var router = require("express").Router();
var Reserva = require("../../models/reserva");

// CRUD (SQL) = EXPRESS
// CREATE (INSERT) = POST
// READ (SELECT) = GET
// UPDATE (UPDATE) = PUT
// DELETE (DELETE) = DELETE
//------------ GET ---------------------
router.get("/", function(req,res, next){
     if (req.auth) {
    	Reserva.find()
    	.populate('menu')
    	.exec(function(err,reservas){
    		if(err) {
    			return next(err);
    		}
    		res.json(reservas);
    	});
     } else {
         res.status(401).json({"missatge":"No autoritzat"});
     }
});

//------ GET --------- /id/:id -----------------------
router.get("/_id/:id", function(req,res,next){
    console.log(req.params.id);
    console.log(req.auth);
    Reserva.find({"_id": req.params.id})
        .populate('menu')
        .populate('disponibilitat')
        .exec(function(err, reserva) {
        if (err) {
            return next(err);
        }
        res.json(reserva);
    });
});

//------------------ POST ----------------------
router.post("/", function (req,res, next) {
        var reserva = new Reserva({
            nombre : req.body.nombre,
            contacto : req.body.contacto,
            numpersonas : req.body.numpersonas,
            menu : req.body.menu,
            dia : req.body.dia,
            turno : req.body.turno,
            turno_id : req.body.turno_id,
            importe : req.body.importe
        });
        reserva.save(function(err, reserva){
            if(err) {return next(err)}
            res.status(201).json(reserva);
        });
});


//------ DELETE --------- /:id -----------------------
router.delete("/:id", function(req, res, next) {
    if (req.auth) {
        Reserva.remove({"_id": req.params.id}, function(err) {
            if (err) {
                return next(err);
            }
            res.json({"missatge": "Reserva amb id " + req.params.id + " esborrat"});
        });
     } else {
         res.status(401).json({"missatge":"No autoritzat"});
     }
});

// ---------------- PUT ---------
router.put("/", function(req, res,next) {
     if (req.auth) {
        console.log(req.body._id);
        Reserva.findByIdAndUpdate(req.body._id,req.body,function(err, reserva) {
            if(err) {
                return next(err);
            }
            res.json({"missatge": "Disponibilitat modificat"});
        });
     } else {
         res.status(401).json({"missatge":"No autoritzat"});
     }
});
module.exports = router;