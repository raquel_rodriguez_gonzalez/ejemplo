// --- VARIABLES GLOBALES ----
var router = require("express").Router();
var Inicio = require("../../models/inicio");

// CRUD (SQL) = EXPRESS
// CREATE (INSERT) = POST
// READ (SELECT) = GET
// UPDATE (UPDATE) = PUT
// DELETE (DELETE) = DELETE
//------------ GET ---------------------
router.get("/", function(req,res, next){
	Inicio.find(function(err,menus){
		if(err) {
			return next(err);
		}
		res.json(menus);
	});
});

//------ GET --------- /id/:id -----------------------
router.get("/_id/:id", function(req,res,next){
    console.log(req.params.id);
    Inicio.find({"_id": req.params.id}).find(function(err, inicio) {
        if (err) {
            return next(err);
        }
        res.json(inicio);
    });
});

//------------------ POST ----------------------
router.post("/", function (req,res, next) {
        var inicio = new Inicio({
            nombre : req.body.nombre,
            informacion : req.body.informacion
        });
        inicio.save(function(err, inicio){
            if(err) {return next(err)}
            res.status(201).json(inicio);
        });
	
});


//------ DELETE --------- /:id -----------------------
router.delete("/:id", function(req, res, next) {
    Inicio.remove({"_id": req.params.id}, function(err) {
        if (err) {
            return next(err);
        }
        res.json({"missatge": "Menu amb id " + req.params.id + " esborrat"});
    });
});

// ---------------- PUT ---------
router.put("/", function(req, res,next) {
    console.log(req.body._id);
    Inicio.findByIdAndUpdate(req.body._id,req.body,function(err, inicio) {
        if(err) {
            return next(err);
        }
        res.json({"missatge": "Menu modificat"});
    });
});
module.exports = router;