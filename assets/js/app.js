    // ---- VARIABLES GLOBALES ----
var app = angular.module('ReservasOnline',["ngResource","ngRoute","ngCookies","angularFileUpload","ngSanitize"]);
angular.module('ReservasOnline')
    .service('CanalSRV', function($rootScope) {
        var socket = io().connect();
        socket.on('connect',function(s) {
        });
    });
angular.module('ReservasOnline')
    .controller("LoginController", function($scope,$location,UserSvc) {
         $scope.$watchGroup(['username','password'],function(newVal, oldVal) {
                if (newVal!=oldVal)
                    $scope.error=null;
                
            });
        $scope.login = function(username,password) {
            if (!username || !password) {
                $scope.error = "Has d'emplenar tots els camps";
            } else{
                console.log(UserSvc);
                UserSvc.login(username,password,
                    function(error,status) {
                        if (status == 401) {
                                $scope.error = error.missatge;
                        }
                    }).success(function() {
                        
                        UserSvc.getUser().then(function(user){

                            $scope.$emit('login', user.data);
                            $location.path('/administracion');
                        });
                    });
            }
        };
    });
// ---- RESOURCE ANGULAR -------
angular.module('ReservasOnline').factory("MailingFactory", function($resource) {
    return {
        svr : $resource("/api/mailing/:id", null,
        {
            'update': { method:'PUT' }
        })
    }
    
});
angular.module('ReservasOnline')
    .controller("RegistreController", function($scope,$location,UserSvc) {
        
        $scope.registre = function(username,password,password2) {
            
            $scope.$watchGroup(['username','password','password2'],function(newVal, oldVal) {
                if (newVal!=oldVal)
                    $scope.error=null;
                
            });
            if (!password || !password2 || !username){
                $scope.error = "Has d'emplenar tots els camps";
                
            }else if (password === password2){
                UserSvc.registre(username,password)
                    .success(function(user) {
                        $location.path('/login');
                    })
                    .error(function(error,status){
                        if (status == 409)
                            $scope.error = error.missatge;
                    });
            } else {
                $scope.error = "Les contrasenyes no són iguals";
            }
        };
    });
    
angular.module('ReservasOnline')
    .config(function($routeProvider, $locationProvider) {
        $routeProvider
            .when("/", {
                controller: 'ApplicationController',
                templateUrl: '/inicio.html',
                autoritzat: false
            })
            .when("/reservas", {
                controller: 'ReservasController',
                templateUrl: '/Reserva/reservas.html',
                autoritzat: true
            })
            .when("/administracion", {
                controller: 'ApplicationController',
                templateUrl: '/administracion.html',
                autoritzat: false
            })
            .when("/dias", {
                controller: 'DispController',
                templateUrl: '/Disponibilidad/disponibilitat.html',
                autoritzat: false
            })
            .when("/noudia", {
                controller: "NouDispController",
                templateUrl: '/Disponibilidad/nouDisp.html',
                autoritzat: false
            })
            .when("/editardisp", {
                controller: "EditarDispController",
                templateUrl: '/Disponibilidad/editarDisp.html',
                autoritzat: false
            })
            .when("/menus", {
                controller: 'MenusController',
                templateUrl: '/Menu/menus.html',
                autoritzat: false
            })
            .when("/noumenu", {
                controller: "NouMenuController",
                templateUrl: '/Menu/nouMenu.html',
                autoritzat: false
            })
            .when("/editarmenu", {
                controller: "EditarMenuController",
                templateUrl: '/Menu/editarMenu.html',
                autoritzat: true
            })
            .when("/noureserva", {
                controller: "NouReservaController",
                templateUrl: '/Reserva/nouReserva.html',
                autoritzat: false
            })
            .when("/editarReserva", {
                controller: "EditarReservaController",
                templateUrl: '/Reserva/editarReserva.html',
                autoritzat: true
            })
            .when("/login", {
                controller: "LoginController",
                templateUrl: "/login/login.html",
                autoritzat: false
            })
            .when("/administracion", {
                controller: 'mostrarImatgesCtrl',
                templateUrl: '/Imagenes/listaImagenes.html',
                autoritzat: true
            })
            .when("/cargarimagenes", {
                controller: 'SubirImatgeCtrl',
                templateUrl: '/Imagenes/cargarImagenes.html',
                autoritzat: true
            })
            .otherwise({
                redirectTo: '/'
            });
            
            $locationProvider.html5Mode({
                enabled: true,
                requireBase: false
            });
    })
    .run(function($rootScope,UserSvc) {
        $rootScope.$on('$routeChangeStart', function(event, next) {
           if (next)
                if (!UserSvc.auth & next.autoritzat) 
                    event.preventDefault();
        });
    });
angular.module('ReservasOnline')
    .service('UserSvc', function($http, $cookies,$rootScope) {
        var svc = this;
        svc.auth= false;
        
        svc.cookie = function(token) {
          $http.defaults.headers.common['x-auth'] = token;
          return $http.get('/api/users').success(function(e){
              svc.auth = true;
              $rootScope.$broadcast('login',e);
          });
        };
        
        if ($cookies["cookie"]) {
            var token = $cookies["cookie"];
            svc.cookie(token);
        }
        svc.getUser = function() {
            return $http.get('/api/users');
        };
        svc.login = function (username, password,noLogin) {
            return $http.post('/api/sessions', {
                username: username,
                password: password
            }).success(function(data,status) {
                svc.token = data;
                $http.defaults.headers.common['x-auth'] = data;
                if (data) svc.auth = true;
            }).error(function(error,status){
                noLogin(error, status);
            });
        };
        svc.registre = function(username,password){
            return $http.post('/api/users', {
                username: username,
                password: password
            });
        };
        svc.logOut = function() {
            svc.auth = false;
            $http.defaults.headers.common['x-auth'] ="";
            delete $cookies["cookie"];
        };
    });
angular.module('ReservasOnline').directive('slider',function() {
        return {
            link: function (scope,element, attrs) {
            }
        };
})
.directive('repeatDone', function() {
		return function(scope, element, attrs) {
			if (scope.$last) { // all are rendered
				scope.$eval(attrs.repeatDone);
			}
		};
	});
angular.module('ReservasOnline')
 .directive('calendarinou', function(ReservasFactory) {
        return {
          restrict: 'E',
          template: '<div class="col-md-6" style="height:200px;"id="calendarnou"></div>',
          link: function (scope, element) {
                g_calendarObject = new JsDatePick({
                        useMode:1,
                        isStripped:true,
                        target:"calendarnou",
                	  cellColorScheme:"aqua"
                    });
                    g_calendarObject.setOnSelectedDelegate(function(){
                        var obj = g_calendarObject.getSelectedDay();
                        ReservasFactory.CogerDia(obj.day + "/" + obj.month + "/" + obj.year);
                        //alert("a date was just selected and the date is : " + obj.day + "/" + obj.month + "/" + obj.year);
                    });
                    ReservasFactory.CogerCalendario($("div[class^='day']")); 
                  }
            };
      });
angular.module('ReservasOnline')
 .directive('calendarillista', function(ReservasFactory) {
        return {
          restrict: 'E',
          template: '<div class="col-md-6" style="height:200px;"id="calendarillista"></div>',
          link: function (scope, element) {
                g_calendarObject = new JsDatePick({
                        useMode:1,
                        isStripped:true,
                        target:"calendarillista",
                	  cellColorScheme:"armygreen"
                    });
                g_calendarObject.setOnSelectedDelegate(function(){
                        var obj = g_calendarObject.getSelectedDay();
                        ReservasFactory.CogerDia(obj.day + "/" + obj.month + "/" + obj.year);
                    });
                    ReservasFactory.CogerCalendario($("div[class^='day']"));                    
                    
                  }
            };
      });
angular.module('ReservasOnline').controller('DispController',function($scope,$location,DispsFactory) {
    $scope.disps = [];

    // ---- GET -----

    DispsFactory.svr.query(function(disps){
        $scope.disps = disps;
        
    });

    $scope.editarDisp = function(disp) {
        DispsFactory.edit = disp;
        $location.path('/editardisp');
    };

    $scope.eliminarDisp = function(disp) {
        console.log(disp._id);
        DispsFactory.svr.delete({id:disp._id},function(l){
            console.log($scope.disps.indexOf(disp));
            $scope.disps.splice($scope.disps.indexOf(disp),1);
        });
        console.log(disp);
    };
});
// ---- RESOURCE ANGULAR -------
angular.module('ReservasOnline').factory("DispsFactory", function($resource) {
    return {
        svr : $resource("/api/disp/:id", null,
        {
            'update': { method:'PUT' }
        }),
        disponible : $resource("/api/disp/disponibles", null),
        edit : null,
        
    }
    
});
angular.module('ReservasOnline').controller('EditarDispController',function($scope,$location,DispsFactory) {
            $scope.editDia = DispsFactory.edit.dia;
            $scope.editPlazas = DispsFactory.edit.plazas;

$scope.updateDisp = function() {
        if ($scope.editDia && $scope.editPlazas) {
            DispsFactory.svr.update({"_id":DispsFactory.edit._id},
                function() {
                $location.path('/');
            });
        }
    };
});
angular.module('ReservasOnline').controller('NouDispController',function($scope,DispsFactory,$location) {
var ultimomes = null;
    DispsFactory.svr.query(function(disps){
        $scope.disps = disps;
        var n = $scope.disps.length - 1;
        var ultimodia = new Date($scope.disps[n].dia);
        ultimomes = (ultimodia.getMonth() + 1);
    });
    $scope.afegirMes = function(dias,mes,ano){
        console.log(ultimomes);
        if (mes > ultimomes){
            var dias = parseInt(dias);
            for (var i = 1; i< dias+1;i++){
            DispsFactory.svr.save({
                "dia" : mes+"/"+i+"/"+ano,
                 "turno" : [{
                   "nombre" : "Mañana",
                   "plazas" : 90
                 },{
                   "nombre" : "MedioDia",
                   "plazas" : 90  
                 },{
                   "nombre" : "Noche",
                   "plazas" : 90   
                 }]
                });
            }
            $location.path('/administracion');
        } else {
            console.log("NO PUEDES ENTRAR ESTE MES!");
        }
    };
});
angular.module('ReservasOnline').controller("mostrarImatgesCtrl",function($scope,imatgesService) {
    imatgesService.fetch()
        .success(function(imatges){
            $scope.imatgesmenus = imatges;
        });
        
        
        $scope.eliminarImatge = function (imatge) {
            imatgesService.delete(imatge);
            $scope.imatges.splice($scope.imatges.indexOf(imatge),1);
        } 
        
});
angular.module('ReservasOnline').controller("SubirImatgeCtrl", function($scope,FileUploader) {
    var uploader = $scope.uploader = new FileUploader({url:"/api/imatges",alias:"image",removeAfterUpload: true});
    //$scope.uploader = $fileUploader.create({url:"/api/imatges",alias:"image",removeAfterUpload: true});
   /* uploader.bind('beforeupload', function (event, item) {
            item.formData.push({some: 'data'});
        });*/
    uploader.onBeforeUploadItem = function (item) {
        item.formData.push({titol: $scope.titol});
        item.formData.push({descripcio: $scope.descripcio});
        }   ;
});
angular.module('ReservasOnline')
    .service("imatgesServiceMenus", function($http) {
        this.fetch = function() {
            return $http.get("/api/imagenesmenus");
        };
        
        this.delete = function(imatge) {
            console.log(imatge._id);
            return $http.delete("/api/imagenesmenus/"+imatge._id);
        };
    });
angular.module('ReservasOnline')
    .service("imatgesService", function($http) {
        this.fetch = function() {
            return $http.get("/api/imatges");
        };
        
        this.delete = function(imatge) {
            console.log(imatge._id);
            return $http.delete("/api/imatges/"+imatge._id);
        };
    });
angular.module('ReservasOnline')
.filter("nl2br", function($filter) {
 return function(data) {
   if (!data) return data;
   return data.replace(/\n\r?/g, '<br />');
 };
})
    .controller("ApplicationController", function($interval,$scope,$location,UserSvc,$cookies,imatgesService,InicioFactory) {
    InicioFactory.svr.query(function(inicio){
        $scope.inicio = inicio;
    });
    
    $scope.ModificarInformacion = function(){
            InicioFactory.svr.update({"_id":$scope.inicio[0]._id,
                                        "nombre":  $scope.inicio[0].nombre,
                                        "informacion": $scope.inicio[0].informacion
            });        
    };
        $scope.$on('login', function(e,user) {
            $scope.currentUser = user;
        });
        $scope.logout = function(){
            UserSvc.logOut();
            delete $scope.currentUser;
            $location.path('/');
        };


    imatgesService.fetch()
        .success(function(imatges){
            $scope.imatges = imatges;
        });
        
        
    $scope.layoutDone = function() {
        $('.slider').slider();
    };
    

    });
// ---- RESOURCE ANGULAR -------
angular.module('ReservasOnline').factory("InicioFactory", function($resource) {
    return {
        svr : $resource("/api/inici/:id", null,
        {
            'update': { method:'PUT' }
        })
        ,
        edit : null
    }
    
});
angular.module('ReservasOnline').controller('EditarMenuController',function($scope,$location,MenusFactory) {
            $scope.editNombre = MenusFactory.edit.nombre;
            $scope.editPrimeros = MenusFactory.edit.primeros;
            $scope.editSegundos = MenusFactory.edit.segundos;
            $scope.editPrecio = MenusFactory.edit.precio;

$scope.updateMenu = function() {
        if ($scope.editNombre && $scope.editPrimeros && $scope.editSegundos 
        && $scope.editPrecio) {
            MenusFactory.svr.update({"_id":MenusFactory.edit._id,
                                        "nombre":  $scope.editNombre,
                                        "primeros": $scope.editPrimeros,
                                        "segundos": $scope.editSegundos,
                                        "precio": $scope.editPrecio
            },
                function() {
                $location.path('/menus');
            });
        }
    };
});
angular.module('ReservasOnline').controller('MenusController',function($scope,$location,MenusFactory,imatgesServiceMenus) {
    $scope.menus = [];

    // ---- GET -----
    imatgesServiceMenus.fetch()
        .success(function(imatges){
            $scope.imatges = imatges;
    });


    MenusFactory.svr.query(function(menus){
        $scope.menus = menus;
    });

    $scope.editarMenu = function(menu) {
        MenusFactory.edit = menu;
        $location.path('/editarmenu');
    };

    $scope.eliminarMenu = function(menu) {
        console.log(menu._id);
        MenusFactory.svr.delete({id:menu._id},function(l){
            console.log($scope.menus.indexOf(menu));
            $scope.menus.splice($scope.menus.indexOf(menu),1);
        });
        console.log(menu);
    };
});
// ---- RESOURCE ANGULAR -------
angular.module('ReservasOnline').factory("MenusFactory", function($resource) {
    return {
        svr : $resource("/api/menu/:id", null,
        {
            'update': { method:'PUT' }
        })
        ,
        edit : null
    }
    
});
angular.module('ReservasOnline').controller('NouMenuController',function($scope,MenusFactory, $location,FileUploader) {
    var uploader = $scope.uploader = new FileUploader({url:"/api/menu",alias:"foto",removeAfterUpload: true});
        uploader.onBeforeUploadItem = function (item) {
            item.formData.push({titol: ""});
            item.formData.push({descripcion: ""});
            item.formData.push({nombre: $scope.nombre});
            item.formData.push({primeros: $scope.primeros});
            item.formData.push({segundos: $scope.segundos});
            item.formData.push({precio: $scope.precio});
        };
        
        $scope.afegirMenu = function() {
        if ($scope.Nombre && $scope.Primeros && $scope.Segundos 
        && $scope.Precio) {
            MenusFactory.svr.save({
                nombre : $scope.Nombre,
                primeros: $scope.Primeros,
                segundos: $scope.Segundos,
                precio: $scope.Precio,
                foto : $scope.foto
            }, function(autor) {
                $scope.Nombre = null;
                $scope.Primeros = null;
                $scope.Segundos = null;
                $scope.Precio = null;
                $scope.foto = null;
                $location.path('/menus');
            });
        }
    };
});
angular.module('ReservasOnline').controller('EditarReservaController',function($scope,$interval,$location,ReservasFactory, MenusFactory, DispsFactory) {
$scope.editNombre = ReservasFactory.edit.nombre;
$scope.editContacto = ReservasFactory.edit.contacto;
$scope.editNumPersonas = ReservasFactory.edit.numpersonas;
$scope.editImporte = ReservasFactory.edit.importe;
$scope.menus = [];
$scope.disps = [];
$scope.turnos = [];
var Antdia = ReservasFactory.edit.dia;
var AntTurno_id = ReservasFactory.edit.turno_id;
var AntNumPersonas = ReservasFactory.edit.numpersonas;
var dia = null;
var entrado = null;
var turno = null;
var algo = true;
        var stop;
          stop = $interval(function() {
              if (algo){
                  algo = false;
                $('select').material_select();
              }
          }, 500);



        MenusFactory.svr.query(function(menus){
            $scope.menus = menus;
        });
        DispsFactory.svr.query(function(disps){
            $scope.disps = disps;
        });
        $scope.$watch('editMenu', function(newVal) {
            if ($scope.editMenu && $scope.editNumPersonas){
            $scope.menus.forEach(function(e,i){
                if (e._id == $scope.editMenu._id) {
                    $scope.editImporte = e.precio * $scope.editNumPersonas;
                }    
            });
            } else {
               $scope.editImporte = 0; 
            }
        });
        $scope.$on('CogerDia', function(){
            entrado = ReservasFactory.dia;
            $scope.disps.forEach(function(e,i){
                var nuevo = new Date(Date.parse(e.dia));
                dia = (nuevo.getDate()+"/"+(nuevo.getMonth()+1)+"/"+nuevo.getFullYear());
                if (dia == entrado){
                    $scope.turnos = e.turno;
                    $scope.editDia = e._id;
                    $scope.$apply();
                    $('select').material_select();
                }
            });
        });
        $scope.$watch('editTurno', function(newVal) {
            $scope.turnos.forEach(function(e,i){
                if (e._id == $scope.editTurno._id){
                    turno = e.nombre;
                    $scope.plazas = e.plazas;
                    if($scope.plazas < $scope.editNumPersonas) {
                        $scope.disponible = true;
                    } else {
                        $scope.disponible = false;
                    }
                }
            });
        });


$scope.updateReserva = function() {
        if ($scope.editNombre && $scope.editContacto && $scope.editNumPersonas
        && $scope.editMenu) {
            ReservasFactory.svr.update({"_id":ReservasFactory.edit._id,
                                        "nombre":  $scope.editNombre,
                                        "contacto": $scope.editContacto,
                                        "numpersonas": $scope.editNumPersonas,
                                        "menu": $scope.editMenu._id,
                                        "dia" : $scope.editDia,
                                        "turno" : turno,
                                        "turno_id" : $scope.editTurno._id,
                                        "importe" : $scope.editImporte
            },
                function() {
                    DispsFactory.svr.update({"_id":Antdia,"turno_id":AntTurno_id,"plazas" : AntNumPersonas,"accion" : "mas"});
                    DispsFactory.svr.update({"_id":$scope.editDia,"turno_id":$scope.editTurno._id,"plazas" : $scope.editNumPersonas,"accion" : "menos"});
                $location.path('/reservas');
            });
        }
    };
});
angular.module('ReservasOnline').controller('NouReservaController',function(imatgesServiceMenus,$rootScope,$interval,$scope,ReservasFactory,$location, MenusFactory,DispsFactory, MailingFactory) {
$scope.menus = [];
$scope.disps = [];
$scope.turnos = [];
$scope.plazas;
$scope.disponible = false;
$scope.Importe = 0;
$scope.mostrar = false;
var codigoValidar = "1234";

var Noejecutado = true;
        var stop;
          stop = $interval(function() {
              if (Noejecutado){
                  Noejecutado = false;
                $('select').material_select();
                var calendario = ReservasFactory.calendario;
                DispsFactory.disponible.query(function(disps){
                    ComprovarDisponibilidad(disps,calendario);
                });
              }
          }, 500);
var dia = null;
var entrado = null;
var turno = null;
    imatgesServiceMenus.fetch()
        .success(function(imatges){
            $scope.imatges = imatges;
    });
        MenusFactory.svr.query(function(menus){
            $scope.menus = menus;
        });
        DispsFactory.disponible.query(function(disps){
            $scope.disps = disps;
        });
        $scope.$watch('Menu', function(newVal) {
            if ($scope.Menu && $scope.NumPersonas){
            $scope.menus.forEach(function(e,i){
                if (e._id == $scope.Menu._id) {
                    $scope.Importe = e.precio * $scope.NumPersonas;
                }    
            });
            } else {
               $scope.Importe = 0; 
            }
        });
        $scope.$on('CogerDia', function(){
            entrado = ReservasFactory.dia;
            $scope.encontrado = false;
            $scope.disps.forEach(function(e,i){
                var nuevo = new Date(Date.parse(e.dia));
                dia = (nuevo.getDate()+"/"+(nuevo.getMonth()+1)+"/"+nuevo.getFullYear());
                if (dia == entrado){
                    $scope.Turno = null;
                    $scope.turnos = e.turno;
                    $scope.Dia = e._id;
                    $scope.encontrado = true;
                    $scope.mostrar = true;
                    console.log("mostrar: "+$scope.mostrar);
                    $scope.$apply();
                    $('select').material_select();
                }
            });
            if (!$scope.encontrado){
                $scope.turnos = [];
                $scope.mostrar = false;
            }
        });
        var correcto;
        $scope.$watch('NumPersonas',function(newVal)   {
            if (!isNaN($scope.NumPersonas)){
                $scope.correcto = true;
            } else {
                $scope.correcto = false;
            }
        })
        $scope.$watch('Turno', function(newVal) {
            $scope.turnos.forEach(function(e,i){
                if (e._id == $scope.Turno._id){
                    turno = e.nombre;
                    $scope.plazas = e.plazas;
                    if($scope.plazas < $scope.NumPersonas) {
                        $scope.disponible = true;
                    } else {
                        $scope.disponible = false;
                    }
                }
            });
        });

        $scope.afegirReserva = function() {
            console.log (codigoValidar == $scope.Codigo);
            if (codigoValidar == $scope.Codigo || $scope.currentUser){
                

        if ($scope.Nombre && $scope.Contacto && $scope.correcto
        && $scope.Menu && $scope.Dia ) {
            ReservasFactory.svr.save({
                nombre : $scope.Nombre,
                contacto: $scope.Contacto,
                numpersonas : $scope.NumPersonas,
                menu : $scope.Menu._id,
                dia : $scope.Dia,
                turno : turno,
                turno_id : $scope.Turno._id,
                importe: $scope.Importe,
            }, function(reserva) {
                DispsFactory.svr.update({"_id":$scope.Dia,"turno_id":$scope.Turno._id,"plazas" : $scope.NumPersonas,"accion" : "menos"});
                $scope.Nombre = null;
                $scope.Contacto = null;
                $scope.NumPersonas = null;
                $scope.Menu = null;
                $scope.Dia = null;
                turno = null;
                $scope.Turno = null;
                $scope.Importe = null;
                if ($scope.currentUser) {
                    $location.path('/reservas');
                } else {
                    $location.path('/');
                }

            });
        } else {  console.log("DATOS INCORRECTO O INCOMPLETOS")
            }
    } else {
                console.log("CODIGO INCORRECTO");
            }
    };
    
    function ComprovarDisponibilidad(disponibles,calendario){
        for (var i = 0; i < calendario.length; i++){        
            var disponible = false;
        var c = calendario[i];
        var diac = $(c).text()+ "/"+g_calendarObject['currentMonth']+"/"+g_calendarObject['currentYear'];
        disponibles.forEach(function(e,i){
            var nuevo = new Date(Date.parse(e.dia));
            dia = (nuevo.getDate()+"/"+(nuevo.getMonth()+1)+"/"+nuevo.getFullYear());
            if (dia == diac) {
                disponible = true;
            }
        });
        if (disponible){
            $(c).attr("disponible",true);
        } else {
            $(c).attr("disponible",false);
            $(c).css("background","url(https://projectofinal-rrodriguez23.c9.io/img/orange_dayNormal.gif) 0% 0% no-repeat"); 
        }
        //console.log(disponible);
        }
    }
    
    
    $scope.EnviarCodigo = function() {
        codigoValidar = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) { 
            var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8); return v.toString(16);
        });

        MailingFactory.svr.save({
            "email" : $scope.Email,
            "codigo" : codigoValidar
        });
    }
});
angular.module('ReservasOnline').controller('ReservasController',function($interval,$scope,$location,ReservasFactory, MenusFactory,DispsFactory) {
    $scope.reservas = [];
    $scope.turnos = [];
    var dia = null;
    var algo = null;
    $scope.reservasm = [];
    $scope.calendario = [];
    // ---- GET -----
    $scope.menus = [];
    MenusFactory.svr.query(function(menus){
            $scope.menus = menus;
    });

var Noejecutado = true;
        var stop;
          stop = $interval(function() {
              if (Noejecutado){
                Noejecutado = false;
                
                var calendario = ReservasFactory.calendario;
                DispsFactory.disponible.query(function(disps){
                    ComprovarDisponibilidad(disps,calendario);
                });
            }
          }, 5);
$scope.disps = [];
    DispsFactory.svr.query(function(disps){
            $scope.disps = disps;
            var f = new Date();
            var hoy = f.getDay() + "/" + (f.getMonth() + 1) + "/" + f.getFullYear();
            console.log(hoy);
            disps.forEach(function(e,i){
                var d = new Date(e.dia); 
                var dia = d.getDate() + "/" + (d.getMonth() +1) + "/" + d.getFullYear();
                if (f > d){
                    if (hoy != dia){
                        console.log(dia);
                        DispsFactory.svr.delete({id:e._id},function(l){
                            console.log($scope.disps.indexOf(e));
                            $scope.disps.splice($scope.disps.indexOf(e),1);
                        });
                    }
                }
            });
        });
        
    ReservasFactory.svr.query(function(reservas){
        $scope.reservas = reservas;
    });
        $scope.$on('CogerDia', function(){
            $scope.reservasm = [];
            $scope.Dia = ReservasFactory.dia;
            $scope.disps.forEach(function(e,i){
                dia = null;
                var nuevo = new Date(Date.parse(e.dia));
                dia = (nuevo.getDate()+"/"+(nuevo.getMonth()+1)+"/"+nuevo.getFullYear());
                //console.log("Lista: "+dia);
                //console.log("Entrado: "+$scope.Dia);
                if ($scope.Dia == dia){
                    //console.log(e._id);
                    $scope.reservas.forEach(function(a,i){
                    if (e._id == a.dia){
                        $scope.reservasm.push(a);
                    }
                });
                }
            });
        });
    $scope.editarReserva = function(reserva) {
        ReservasFactory.edit = reserva;
        $location.path('/editarReserva');
    };

    $scope.eliminarReserva = function(reserva) {
        console.log(reserva._id);
        console.log(reserva.numpersonas);
        DispsFactory.svr.update({"_id":reserva.dia,"turno_id":reserva.turno_id,"plazas" : reserva.numpersonas,"accion":"mas"});
        ReservasFactory.svr.delete({id:reserva._id},function(l){
            console.log($scope.reservas.indexOf(reserva));
            $scope.reservas.splice($scope.reservas.indexOf(reserva),1);
            $scope.reservasm.splice($scope.reservasm.indexOf(reserva),1);
        });
        console.log(reserva);
    };

    function ComprovarDisponibilidad(disponibles,calendario){
        for (var i = 0; i < calendario.length; i++){        
            var disponible = false;
        var c = calendario[i];
        var diac = $(c).text()+ "/"+g_calendarObject['currentMonth']+"/"+g_calendarObject['currentYear'];
        disponibles.forEach(function(e,i){
            var nuevo = new Date(Date.parse(e.dia));
            dia = (nuevo.getDate()+"/"+(nuevo.getMonth()+1)+"/"+nuevo.getFullYear());
            if (dia == diac) {
                disponible = true;
            }
        });
        if (disponible){
            $(c).attr("disponible",true);
        } else {
            $(c).attr("disponible",false);
            $(c).css("background","url(https://projectofinal-rrodriguez23.c9.io/img/orange_dayNormal.gif) 0% 0% no-repeat"); 
        }
        }
        $("#calendarillista").attr("actualizar",false);
    }
    
  $scope.ComprovarDisponibilidad = function(){
        console.log("algo");   
  }
});
// ---- RESOURCE ANGULAR -------
angular.module('ReservasOnline').factory("ReservasFactory", function($resource,$rootScope) {
    return {
        svr : $resource("/api/reserva/:id", null,
        {
            'update': { method:'PUT' }
        })
        ,
        edit : null,
        dia : null,
        CogerDia : function(dia){
            this.dia = dia;
            $rootScope.$broadcast('CogerDia');
            $rootScope.$apply();
        },
        calendario : null,
        CogerCalendario : function(calendario){
            this.calendario = calendario;
        }
    }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1vZHVsZS5qcyIsImNhbmFsLnN2Yy5qcyIsImxvZ2luLmN0cmwuanMiLCJtYWlsaW5nLnN2Yy5qcyIsInJlZ2lzdHJlLmN0cmwuanMiLCJyb3V0ZXMuanMiLCJ1c2VyLnN2Yy5qcyIsIkRpcmVjdGl2YXMvU2xpZGVyLmRpci5qcyIsIkRpcmVjdGl2YXMvY2FsZW5kYXIuZGlyLmpzIiwiRGlyZWN0aXZhcy9jYWxlbmRhcmxsaXN0YS5kaXIuanMiLCJEaXNwb25pYmlsaWRhZC9EaXNwLmN0cmwuanMiLCJEaXNwb25pYmlsaWRhZC9EaXNwLnN2Yy5qcyIsIkRpc3BvbmliaWxpZGFkL0VkaXREaXNwb25pYmlsaWRhZC5jdHJsLmpzIiwiRGlzcG9uaWJpbGlkYWQvTm91RGlzcC5jdHJsLmpzIiwiSW1hdGdlcy9Nb3N0cmFySW1hZ2VuZXMuY3RybC5qcyIsIkltYXRnZXMvU3ViaXJJbWFnZW5lcy5jdHJsLmpzIiwiSW1hdGdlcy9tb3N0cmFySW1hZ2VuZXNNZW51cy5zdmMuanMiLCJJbWF0Z2VzL21vc3RyYXRJbWFnZW5lcy5zdmMuanMiLCJJbmljaW8vSW5pY2lvLmN0cmwuanMiLCJJbmljaW8vSW5pY2lvLnN2Yy5qcyIsIk1lbnUvRWRpdE1lbnUuY3RybC5qcyIsIk1lbnUvTWVudXMuY3RybC5qcyIsIk1lbnUvTWVudXMuc3ZjLmpzIiwiTWVudS9Ob3VNZW51LmN0cmwuanMiLCJSZXNlcnZhL0VkaXRSZXNlcnZhLmN0cmwuanMiLCJSZXNlcnZhL05vdVJlc2VydmEuY3RybC5qcyIsIlJlc2VydmEvUmVzZXJ2YXMuanMiLCJSZXNlcnZhL3Jlc2VydmFzLnN2Yy5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FDREE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDTEE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUMzQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUNUQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQzNCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDeEZBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUMzQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUNaQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUNwQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDcEJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ3ZCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ1pBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDWkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDaENBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDWkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ1ZBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUNWQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDVkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUN2Q0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDWEE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDcEJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDM0JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ1hBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDOUJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDMUZBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUNuS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDOUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsImZpbGUiOiJhcHAuanMiLCJzb3VyY2VzQ29udGVudCI6WyIgICAgLy8gLS0tLSBWQVJJQUJMRVMgR0xPQkFMRVMgLS0tLVxudmFyIGFwcCA9IGFuZ3VsYXIubW9kdWxlKCdSZXNlcnZhc09ubGluZScsW1wibmdSZXNvdXJjZVwiLFwibmdSb3V0ZVwiLFwibmdDb29raWVzXCIsXCJhbmd1bGFyRmlsZVVwbG9hZFwiLFwibmdTYW5pdGl6ZVwiXSk7IiwiYW5ndWxhci5tb2R1bGUoJ1Jlc2VydmFzT25saW5lJylcbiAgICAuc2VydmljZSgnQ2FuYWxTUlYnLCBmdW5jdGlvbigkcm9vdFNjb3BlKSB7XG4gICAgICAgIHZhciBzb2NrZXQgPSBpbygpLmNvbm5lY3QoKTtcbiAgICAgICAgc29ja2V0Lm9uKCdjb25uZWN0JyxmdW5jdGlvbihzKSB7XG4gICAgICAgIH0pO1xuICAgIH0pOyIsImFuZ3VsYXIubW9kdWxlKCdSZXNlcnZhc09ubGluZScpXG4gICAgLmNvbnRyb2xsZXIoXCJMb2dpbkNvbnRyb2xsZXJcIiwgZnVuY3Rpb24oJHNjb3BlLCRsb2NhdGlvbixVc2VyU3ZjKSB7XG4gICAgICAgICAkc2NvcGUuJHdhdGNoR3JvdXAoWyd1c2VybmFtZScsJ3Bhc3N3b3JkJ10sZnVuY3Rpb24obmV3VmFsLCBvbGRWYWwpIHtcbiAgICAgICAgICAgICAgICBpZiAobmV3VmFsIT1vbGRWYWwpXG4gICAgICAgICAgICAgICAgICAgICRzY29wZS5lcnJvcj1udWxsO1xuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgfSk7XG4gICAgICAgICRzY29wZS5sb2dpbiA9IGZ1bmN0aW9uKHVzZXJuYW1lLHBhc3N3b3JkKSB7XG4gICAgICAgICAgICBpZiAoIXVzZXJuYW1lIHx8ICFwYXNzd29yZCkge1xuICAgICAgICAgICAgICAgICRzY29wZS5lcnJvciA9IFwiSGFzIGQnZW1wbGVuYXIgdG90cyBlbHMgY2FtcHNcIjtcbiAgICAgICAgICAgIH0gZWxzZXtcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhVc2VyU3ZjKTtcbiAgICAgICAgICAgICAgICBVc2VyU3ZjLmxvZ2luKHVzZXJuYW1lLHBhc3N3b3JkLFxuICAgICAgICAgICAgICAgICAgICBmdW5jdGlvbihlcnJvcixzdGF0dXMpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChzdGF0dXMgPT0gNDAxKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICRzY29wZS5lcnJvciA9IGVycm9yLm1pc3NhdGdlO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9KS5zdWNjZXNzKGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgICAgICAgICBVc2VyU3ZjLmdldFVzZXIoKS50aGVuKGZ1bmN0aW9uKHVzZXIpe1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJHNjb3BlLiRlbWl0KCdsb2dpbicsIHVzZXIuZGF0YSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJGxvY2F0aW9uLnBhdGgoJy9hZG1pbmlzdHJhY2lvbicpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfVxuICAgICAgICB9O1xuICAgIH0pOyIsIi8vIC0tLS0gUkVTT1VSQ0UgQU5HVUxBUiAtLS0tLS0tXG5hbmd1bGFyLm1vZHVsZSgnUmVzZXJ2YXNPbmxpbmUnKS5mYWN0b3J5KFwiTWFpbGluZ0ZhY3RvcnlcIiwgZnVuY3Rpb24oJHJlc291cmNlKSB7XG4gICAgcmV0dXJuIHtcbiAgICAgICAgc3ZyIDogJHJlc291cmNlKFwiL2FwaS9tYWlsaW5nLzppZFwiLCBudWxsLFxuICAgICAgICB7XG4gICAgICAgICAgICAndXBkYXRlJzogeyBtZXRob2Q6J1BVVCcgfVxuICAgICAgICB9KVxuICAgIH1cbiAgICBcbn0pOyIsImFuZ3VsYXIubW9kdWxlKCdSZXNlcnZhc09ubGluZScpXG4gICAgLmNvbnRyb2xsZXIoXCJSZWdpc3RyZUNvbnRyb2xsZXJcIiwgZnVuY3Rpb24oJHNjb3BlLCRsb2NhdGlvbixVc2VyU3ZjKSB7XG4gICAgICAgIFxuICAgICAgICAkc2NvcGUucmVnaXN0cmUgPSBmdW5jdGlvbih1c2VybmFtZSxwYXNzd29yZCxwYXNzd29yZDIpIHtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgJHNjb3BlLiR3YXRjaEdyb3VwKFsndXNlcm5hbWUnLCdwYXNzd29yZCcsJ3Bhc3N3b3JkMiddLGZ1bmN0aW9uKG5ld1ZhbCwgb2xkVmFsKSB7XG4gICAgICAgICAgICAgICAgaWYgKG5ld1ZhbCE9b2xkVmFsKVxuICAgICAgICAgICAgICAgICAgICAkc2NvcGUuZXJyb3I9bnVsbDtcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgaWYgKCFwYXNzd29yZCB8fCAhcGFzc3dvcmQyIHx8ICF1c2VybmFtZSl7XG4gICAgICAgICAgICAgICAgJHNjb3BlLmVycm9yID0gXCJIYXMgZCdlbXBsZW5hciB0b3RzIGVscyBjYW1wc1wiO1xuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgfWVsc2UgaWYgKHBhc3N3b3JkID09PSBwYXNzd29yZDIpe1xuICAgICAgICAgICAgICAgIFVzZXJTdmMucmVnaXN0cmUodXNlcm5hbWUscGFzc3dvcmQpXG4gICAgICAgICAgICAgICAgICAgIC5zdWNjZXNzKGZ1bmN0aW9uKHVzZXIpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICRsb2NhdGlvbi5wYXRoKCcvbG9naW4nKTtcbiAgICAgICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAgICAgLmVycm9yKGZ1bmN0aW9uKGVycm9yLHN0YXR1cyl7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoc3RhdHVzID09IDQwOSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkc2NvcGUuZXJyb3IgPSBlcnJvci5taXNzYXRnZTtcbiAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICRzY29wZS5lcnJvciA9IFwiTGVzIGNvbnRyYXNlbnllcyBubyBzw7NuIGlndWFsc1wiO1xuICAgICAgICAgICAgfVxuICAgICAgICB9O1xuICAgIH0pO1xuICAgICIsImFuZ3VsYXIubW9kdWxlKCdSZXNlcnZhc09ubGluZScpXG4gICAgLmNvbmZpZyhmdW5jdGlvbigkcm91dGVQcm92aWRlciwgJGxvY2F0aW9uUHJvdmlkZXIpIHtcbiAgICAgICAgJHJvdXRlUHJvdmlkZXJcbiAgICAgICAgICAgIC53aGVuKFwiL1wiLCB7XG4gICAgICAgICAgICAgICAgY29udHJvbGxlcjogJ0FwcGxpY2F0aW9uQ29udHJvbGxlcicsXG4gICAgICAgICAgICAgICAgdGVtcGxhdGVVcmw6ICcvaW5pY2lvLmh0bWwnLFxuICAgICAgICAgICAgICAgIGF1dG9yaXR6YXQ6IGZhbHNlXG4gICAgICAgICAgICB9KVxuICAgICAgICAgICAgLndoZW4oXCIvcmVzZXJ2YXNcIiwge1xuICAgICAgICAgICAgICAgIGNvbnRyb2xsZXI6ICdSZXNlcnZhc0NvbnRyb2xsZXInLFxuICAgICAgICAgICAgICAgIHRlbXBsYXRlVXJsOiAnL1Jlc2VydmEvcmVzZXJ2YXMuaHRtbCcsXG4gICAgICAgICAgICAgICAgYXV0b3JpdHphdDogdHJ1ZVxuICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIC53aGVuKFwiL2FkbWluaXN0cmFjaW9uXCIsIHtcbiAgICAgICAgICAgICAgICBjb250cm9sbGVyOiAnQXBwbGljYXRpb25Db250cm9sbGVyJyxcbiAgICAgICAgICAgICAgICB0ZW1wbGF0ZVVybDogJy9hZG1pbmlzdHJhY2lvbi5odG1sJyxcbiAgICAgICAgICAgICAgICBhdXRvcml0emF0OiBmYWxzZVxuICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIC53aGVuKFwiL2RpYXNcIiwge1xuICAgICAgICAgICAgICAgIGNvbnRyb2xsZXI6ICdEaXNwQ29udHJvbGxlcicsXG4gICAgICAgICAgICAgICAgdGVtcGxhdGVVcmw6ICcvRGlzcG9uaWJpbGlkYWQvZGlzcG9uaWJpbGl0YXQuaHRtbCcsXG4gICAgICAgICAgICAgICAgYXV0b3JpdHphdDogZmFsc2VcbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAud2hlbihcIi9ub3VkaWFcIiwge1xuICAgICAgICAgICAgICAgIGNvbnRyb2xsZXI6IFwiTm91RGlzcENvbnRyb2xsZXJcIixcbiAgICAgICAgICAgICAgICB0ZW1wbGF0ZVVybDogJy9EaXNwb25pYmlsaWRhZC9ub3VEaXNwLmh0bWwnLFxuICAgICAgICAgICAgICAgIGF1dG9yaXR6YXQ6IGZhbHNlXG4gICAgICAgICAgICB9KVxuICAgICAgICAgICAgLndoZW4oXCIvZWRpdGFyZGlzcFwiLCB7XG4gICAgICAgICAgICAgICAgY29udHJvbGxlcjogXCJFZGl0YXJEaXNwQ29udHJvbGxlclwiLFxuICAgICAgICAgICAgICAgIHRlbXBsYXRlVXJsOiAnL0Rpc3BvbmliaWxpZGFkL2VkaXRhckRpc3AuaHRtbCcsXG4gICAgICAgICAgICAgICAgYXV0b3JpdHphdDogZmFsc2VcbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAud2hlbihcIi9tZW51c1wiLCB7XG4gICAgICAgICAgICAgICAgY29udHJvbGxlcjogJ01lbnVzQ29udHJvbGxlcicsXG4gICAgICAgICAgICAgICAgdGVtcGxhdGVVcmw6ICcvTWVudS9tZW51cy5odG1sJyxcbiAgICAgICAgICAgICAgICBhdXRvcml0emF0OiBmYWxzZVxuICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIC53aGVuKFwiL25vdW1lbnVcIiwge1xuICAgICAgICAgICAgICAgIGNvbnRyb2xsZXI6IFwiTm91TWVudUNvbnRyb2xsZXJcIixcbiAgICAgICAgICAgICAgICB0ZW1wbGF0ZVVybDogJy9NZW51L25vdU1lbnUuaHRtbCcsXG4gICAgICAgICAgICAgICAgYXV0b3JpdHphdDogZmFsc2VcbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAud2hlbihcIi9lZGl0YXJtZW51XCIsIHtcbiAgICAgICAgICAgICAgICBjb250cm9sbGVyOiBcIkVkaXRhck1lbnVDb250cm9sbGVyXCIsXG4gICAgICAgICAgICAgICAgdGVtcGxhdGVVcmw6ICcvTWVudS9lZGl0YXJNZW51Lmh0bWwnLFxuICAgICAgICAgICAgICAgIGF1dG9yaXR6YXQ6IHRydWVcbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAud2hlbihcIi9ub3VyZXNlcnZhXCIsIHtcbiAgICAgICAgICAgICAgICBjb250cm9sbGVyOiBcIk5vdVJlc2VydmFDb250cm9sbGVyXCIsXG4gICAgICAgICAgICAgICAgdGVtcGxhdGVVcmw6ICcvUmVzZXJ2YS9ub3VSZXNlcnZhLmh0bWwnLFxuICAgICAgICAgICAgICAgIGF1dG9yaXR6YXQ6IGZhbHNlXG4gICAgICAgICAgICB9KVxuICAgICAgICAgICAgLndoZW4oXCIvZWRpdGFyUmVzZXJ2YVwiLCB7XG4gICAgICAgICAgICAgICAgY29udHJvbGxlcjogXCJFZGl0YXJSZXNlcnZhQ29udHJvbGxlclwiLFxuICAgICAgICAgICAgICAgIHRlbXBsYXRlVXJsOiAnL1Jlc2VydmEvZWRpdGFyUmVzZXJ2YS5odG1sJyxcbiAgICAgICAgICAgICAgICBhdXRvcml0emF0OiB0cnVlXG4gICAgICAgICAgICB9KVxuICAgICAgICAgICAgLndoZW4oXCIvbG9naW5cIiwge1xuICAgICAgICAgICAgICAgIGNvbnRyb2xsZXI6IFwiTG9naW5Db250cm9sbGVyXCIsXG4gICAgICAgICAgICAgICAgdGVtcGxhdGVVcmw6IFwiL2xvZ2luL2xvZ2luLmh0bWxcIixcbiAgICAgICAgICAgICAgICBhdXRvcml0emF0OiBmYWxzZVxuICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIC53aGVuKFwiL2FkbWluaXN0cmFjaW9uXCIsIHtcbiAgICAgICAgICAgICAgICBjb250cm9sbGVyOiAnbW9zdHJhckltYXRnZXNDdHJsJyxcbiAgICAgICAgICAgICAgICB0ZW1wbGF0ZVVybDogJy9JbWFnZW5lcy9saXN0YUltYWdlbmVzLmh0bWwnLFxuICAgICAgICAgICAgICAgIGF1dG9yaXR6YXQ6IHRydWVcbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAud2hlbihcIi9jYXJnYXJpbWFnZW5lc1wiLCB7XG4gICAgICAgICAgICAgICAgY29udHJvbGxlcjogJ1N1YmlySW1hdGdlQ3RybCcsXG4gICAgICAgICAgICAgICAgdGVtcGxhdGVVcmw6ICcvSW1hZ2VuZXMvY2FyZ2FySW1hZ2VuZXMuaHRtbCcsXG4gICAgICAgICAgICAgICAgYXV0b3JpdHphdDogdHJ1ZVxuICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIC5vdGhlcndpc2Uoe1xuICAgICAgICAgICAgICAgIHJlZGlyZWN0VG86ICcvJ1xuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgICRsb2NhdGlvblByb3ZpZGVyLmh0bWw1TW9kZSh7XG4gICAgICAgICAgICAgICAgZW5hYmxlZDogdHJ1ZSxcbiAgICAgICAgICAgICAgICByZXF1aXJlQmFzZTogZmFsc2VcbiAgICAgICAgICAgIH0pO1xuICAgIH0pXG4gICAgLnJ1bihmdW5jdGlvbigkcm9vdFNjb3BlLFVzZXJTdmMpIHtcbiAgICAgICAgJHJvb3RTY29wZS4kb24oJyRyb3V0ZUNoYW5nZVN0YXJ0JywgZnVuY3Rpb24oZXZlbnQsIG5leHQpIHtcbiAgICAgICAgICAgaWYgKG5leHQpXG4gICAgICAgICAgICAgICAgaWYgKCFVc2VyU3ZjLmF1dGggJiBuZXh0LmF1dG9yaXR6YXQpIFxuICAgICAgICAgICAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICB9KTtcbiAgICB9KTsiLCJhbmd1bGFyLm1vZHVsZSgnUmVzZXJ2YXNPbmxpbmUnKVxuICAgIC5zZXJ2aWNlKCdVc2VyU3ZjJywgZnVuY3Rpb24oJGh0dHAsICRjb29raWVzLCRyb290U2NvcGUpIHtcbiAgICAgICAgdmFyIHN2YyA9IHRoaXM7XG4gICAgICAgIHN2Yy5hdXRoPSBmYWxzZTtcbiAgICAgICAgXG4gICAgICAgIHN2Yy5jb29raWUgPSBmdW5jdGlvbih0b2tlbikge1xuICAgICAgICAgICRodHRwLmRlZmF1bHRzLmhlYWRlcnMuY29tbW9uWyd4LWF1dGgnXSA9IHRva2VuO1xuICAgICAgICAgIHJldHVybiAkaHR0cC5nZXQoJy9hcGkvdXNlcnMnKS5zdWNjZXNzKGZ1bmN0aW9uKGUpe1xuICAgICAgICAgICAgICBzdmMuYXV0aCA9IHRydWU7XG4gICAgICAgICAgICAgICRyb290U2NvcGUuJGJyb2FkY2FzdCgnbG9naW4nLGUpO1xuICAgICAgICAgIH0pO1xuICAgICAgICB9O1xuICAgICAgICBcbiAgICAgICAgaWYgKCRjb29raWVzW1wiY29va2llXCJdKSB7XG4gICAgICAgICAgICB2YXIgdG9rZW4gPSAkY29va2llc1tcImNvb2tpZVwiXTtcbiAgICAgICAgICAgIHN2Yy5jb29raWUodG9rZW4pO1xuICAgICAgICB9XG4gICAgICAgIHN2Yy5nZXRVc2VyID0gZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICByZXR1cm4gJGh0dHAuZ2V0KCcvYXBpL3VzZXJzJyk7XG4gICAgICAgIH07XG4gICAgICAgIHN2Yy5sb2dpbiA9IGZ1bmN0aW9uICh1c2VybmFtZSwgcGFzc3dvcmQsbm9Mb2dpbikge1xuICAgICAgICAgICAgcmV0dXJuICRodHRwLnBvc3QoJy9hcGkvc2Vzc2lvbnMnLCB7XG4gICAgICAgICAgICAgICAgdXNlcm5hbWU6IHVzZXJuYW1lLFxuICAgICAgICAgICAgICAgIHBhc3N3b3JkOiBwYXNzd29yZFxuICAgICAgICAgICAgfSkuc3VjY2VzcyhmdW5jdGlvbihkYXRhLHN0YXR1cykge1xuICAgICAgICAgICAgICAgIHN2Yy50b2tlbiA9IGRhdGE7XG4gICAgICAgICAgICAgICAgJGh0dHAuZGVmYXVsdHMuaGVhZGVycy5jb21tb25bJ3gtYXV0aCddID0gZGF0YTtcbiAgICAgICAgICAgICAgICBpZiAoZGF0YSkgc3ZjLmF1dGggPSB0cnVlO1xuICAgICAgICAgICAgfSkuZXJyb3IoZnVuY3Rpb24oZXJyb3Isc3RhdHVzKXtcbiAgICAgICAgICAgICAgICBub0xvZ2luKGVycm9yLCBzdGF0dXMpO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH07XG4gICAgICAgIHN2Yy5yZWdpc3RyZSA9IGZ1bmN0aW9uKHVzZXJuYW1lLHBhc3N3b3JkKXtcbiAgICAgICAgICAgIHJldHVybiAkaHR0cC5wb3N0KCcvYXBpL3VzZXJzJywge1xuICAgICAgICAgICAgICAgIHVzZXJuYW1lOiB1c2VybmFtZSxcbiAgICAgICAgICAgICAgICBwYXNzd29yZDogcGFzc3dvcmRcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9O1xuICAgICAgICBzdmMubG9nT3V0ID0gZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBzdmMuYXV0aCA9IGZhbHNlO1xuICAgICAgICAgICAgJGh0dHAuZGVmYXVsdHMuaGVhZGVycy5jb21tb25bJ3gtYXV0aCddID1cIlwiO1xuICAgICAgICAgICAgZGVsZXRlICRjb29raWVzW1wiY29va2llXCJdO1xuICAgICAgICB9O1xuICAgIH0pOyIsImFuZ3VsYXIubW9kdWxlKCdSZXNlcnZhc09ubGluZScpLmRpcmVjdGl2ZSgnc2xpZGVyJyxmdW5jdGlvbigpIHtcbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgIGxpbms6IGZ1bmN0aW9uIChzY29wZSxlbGVtZW50LCBhdHRycykge1xuICAgICAgICAgICAgfVxuICAgICAgICB9O1xufSlcbi5kaXJlY3RpdmUoJ3JlcGVhdERvbmUnLCBmdW5jdGlvbigpIHtcblx0XHRyZXR1cm4gZnVuY3Rpb24oc2NvcGUsIGVsZW1lbnQsIGF0dHJzKSB7XG5cdFx0XHRpZiAoc2NvcGUuJGxhc3QpIHsgLy8gYWxsIGFyZSByZW5kZXJlZFxuXHRcdFx0XHRzY29wZS4kZXZhbChhdHRycy5yZXBlYXREb25lKTtcblx0XHRcdH1cblx0XHR9O1xuXHR9KTsiLCJhbmd1bGFyLm1vZHVsZSgnUmVzZXJ2YXNPbmxpbmUnKVxuIC5kaXJlY3RpdmUoJ2NhbGVuZGFyaW5vdScsIGZ1bmN0aW9uKFJlc2VydmFzRmFjdG9yeSkge1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgIHJlc3RyaWN0OiAnRScsXG4gICAgICAgICAgdGVtcGxhdGU6ICc8ZGl2IGNsYXNzPVwiY29sLW1kLTZcIiBzdHlsZT1cImhlaWdodDoyMDBweDtcImlkPVwiY2FsZW5kYXJub3VcIj48L2Rpdj4nLFxuICAgICAgICAgIGxpbms6IGZ1bmN0aW9uIChzY29wZSwgZWxlbWVudCkge1xuICAgICAgICAgICAgICAgIGdfY2FsZW5kYXJPYmplY3QgPSBuZXcgSnNEYXRlUGljayh7XG4gICAgICAgICAgICAgICAgICAgICAgICB1c2VNb2RlOjEsXG4gICAgICAgICAgICAgICAgICAgICAgICBpc1N0cmlwcGVkOnRydWUsXG4gICAgICAgICAgICAgICAgICAgICAgICB0YXJnZXQ6XCJjYWxlbmRhcm5vdVwiLFxuICAgICAgICAgICAgICAgIFx0ICBjZWxsQ29sb3JTY2hlbWU6XCJhcXVhXCJcbiAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgIGdfY2FsZW5kYXJPYmplY3Quc2V0T25TZWxlY3RlZERlbGVnYXRlKGZ1bmN0aW9uKCl7XG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgb2JqID0gZ19jYWxlbmRhck9iamVjdC5nZXRTZWxlY3RlZERheSgpO1xuICAgICAgICAgICAgICAgICAgICAgICAgUmVzZXJ2YXNGYWN0b3J5LkNvZ2VyRGlhKG9iai5kYXkgKyBcIi9cIiArIG9iai5tb250aCArIFwiL1wiICsgb2JqLnllYXIpO1xuICAgICAgICAgICAgICAgICAgICAgICAgLy9hbGVydChcImEgZGF0ZSB3YXMganVzdCBzZWxlY3RlZCBhbmQgdGhlIGRhdGUgaXMgOiBcIiArIG9iai5kYXkgKyBcIi9cIiArIG9iai5tb250aCArIFwiL1wiICsgb2JqLnllYXIpO1xuICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgUmVzZXJ2YXNGYWN0b3J5LkNvZ2VyQ2FsZW5kYXJpbygkKFwiZGl2W2NsYXNzXj0nZGF5J11cIikpOyBcbiAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH07XG4gICAgICB9KTsiLCJhbmd1bGFyLm1vZHVsZSgnUmVzZXJ2YXNPbmxpbmUnKVxuIC5kaXJlY3RpdmUoJ2NhbGVuZGFyaWxsaXN0YScsIGZ1bmN0aW9uKFJlc2VydmFzRmFjdG9yeSkge1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgIHJlc3RyaWN0OiAnRScsXG4gICAgICAgICAgdGVtcGxhdGU6ICc8ZGl2IGNsYXNzPVwiY29sLW1kLTZcIiBzdHlsZT1cImhlaWdodDoyMDBweDtcImlkPVwiY2FsZW5kYXJpbGxpc3RhXCI+PC9kaXY+JyxcbiAgICAgICAgICBsaW5rOiBmdW5jdGlvbiAoc2NvcGUsIGVsZW1lbnQpIHtcbiAgICAgICAgICAgICAgICBnX2NhbGVuZGFyT2JqZWN0ID0gbmV3IEpzRGF0ZVBpY2soe1xuICAgICAgICAgICAgICAgICAgICAgICAgdXNlTW9kZToxLFxuICAgICAgICAgICAgICAgICAgICAgICAgaXNTdHJpcHBlZDp0cnVlLFxuICAgICAgICAgICAgICAgICAgICAgICAgdGFyZ2V0OlwiY2FsZW5kYXJpbGxpc3RhXCIsXG4gICAgICAgICAgICAgICAgXHQgIGNlbGxDb2xvclNjaGVtZTpcImFybXlncmVlblwiXG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIGdfY2FsZW5kYXJPYmplY3Quc2V0T25TZWxlY3RlZERlbGVnYXRlKGZ1bmN0aW9uKCl7XG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgb2JqID0gZ19jYWxlbmRhck9iamVjdC5nZXRTZWxlY3RlZERheSgpO1xuICAgICAgICAgICAgICAgICAgICAgICAgUmVzZXJ2YXNGYWN0b3J5LkNvZ2VyRGlhKG9iai5kYXkgKyBcIi9cIiArIG9iai5tb250aCArIFwiL1wiICsgb2JqLnllYXIpO1xuICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgUmVzZXJ2YXNGYWN0b3J5LkNvZ2VyQ2FsZW5kYXJpbygkKFwiZGl2W2NsYXNzXj0nZGF5J11cIikpOyAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfTtcbiAgICAgIH0pOyIsImFuZ3VsYXIubW9kdWxlKCdSZXNlcnZhc09ubGluZScpLmNvbnRyb2xsZXIoJ0Rpc3BDb250cm9sbGVyJyxmdW5jdGlvbigkc2NvcGUsJGxvY2F0aW9uLERpc3BzRmFjdG9yeSkge1xuICAgICRzY29wZS5kaXNwcyA9IFtdO1xuXG4gICAgLy8gLS0tLSBHRVQgLS0tLS1cblxuICAgIERpc3BzRmFjdG9yeS5zdnIucXVlcnkoZnVuY3Rpb24oZGlzcHMpe1xuICAgICAgICAkc2NvcGUuZGlzcHMgPSBkaXNwcztcbiAgICAgICAgXG4gICAgfSk7XG5cbiAgICAkc2NvcGUuZWRpdGFyRGlzcCA9IGZ1bmN0aW9uKGRpc3ApIHtcbiAgICAgICAgRGlzcHNGYWN0b3J5LmVkaXQgPSBkaXNwO1xuICAgICAgICAkbG9jYXRpb24ucGF0aCgnL2VkaXRhcmRpc3AnKTtcbiAgICB9O1xuXG4gICAgJHNjb3BlLmVsaW1pbmFyRGlzcCA9IGZ1bmN0aW9uKGRpc3ApIHtcbiAgICAgICAgY29uc29sZS5sb2coZGlzcC5faWQpO1xuICAgICAgICBEaXNwc0ZhY3Rvcnkuc3ZyLmRlbGV0ZSh7aWQ6ZGlzcC5faWR9LGZ1bmN0aW9uKGwpe1xuICAgICAgICAgICAgY29uc29sZS5sb2coJHNjb3BlLmRpc3BzLmluZGV4T2YoZGlzcCkpO1xuICAgICAgICAgICAgJHNjb3BlLmRpc3BzLnNwbGljZSgkc2NvcGUuZGlzcHMuaW5kZXhPZihkaXNwKSwxKTtcbiAgICAgICAgfSk7XG4gICAgICAgIGNvbnNvbGUubG9nKGRpc3ApO1xuICAgIH07XG59KTsiLCIvLyAtLS0tIFJFU09VUkNFIEFOR1VMQVIgLS0tLS0tLVxuYW5ndWxhci5tb2R1bGUoJ1Jlc2VydmFzT25saW5lJykuZmFjdG9yeShcIkRpc3BzRmFjdG9yeVwiLCBmdW5jdGlvbigkcmVzb3VyY2UpIHtcbiAgICByZXR1cm4ge1xuICAgICAgICBzdnIgOiAkcmVzb3VyY2UoXCIvYXBpL2Rpc3AvOmlkXCIsIG51bGwsXG4gICAgICAgIHtcbiAgICAgICAgICAgICd1cGRhdGUnOiB7IG1ldGhvZDonUFVUJyB9XG4gICAgICAgIH0pLFxuICAgICAgICBkaXNwb25pYmxlIDogJHJlc291cmNlKFwiL2FwaS9kaXNwL2Rpc3BvbmlibGVzXCIsIG51bGwpLFxuICAgICAgICBlZGl0IDogbnVsbCxcbiAgICAgICAgXG4gICAgfVxuICAgIFxufSk7IiwiYW5ndWxhci5tb2R1bGUoJ1Jlc2VydmFzT25saW5lJykuY29udHJvbGxlcignRWRpdGFyRGlzcENvbnRyb2xsZXInLGZ1bmN0aW9uKCRzY29wZSwkbG9jYXRpb24sRGlzcHNGYWN0b3J5KSB7XG4gICAgICAgICAgICAkc2NvcGUuZWRpdERpYSA9IERpc3BzRmFjdG9yeS5lZGl0LmRpYTtcbiAgICAgICAgICAgICRzY29wZS5lZGl0UGxhemFzID0gRGlzcHNGYWN0b3J5LmVkaXQucGxhemFzO1xuXG4kc2NvcGUudXBkYXRlRGlzcCA9IGZ1bmN0aW9uKCkge1xuICAgICAgICBpZiAoJHNjb3BlLmVkaXREaWEgJiYgJHNjb3BlLmVkaXRQbGF6YXMpIHtcbiAgICAgICAgICAgIERpc3BzRmFjdG9yeS5zdnIudXBkYXRlKHtcIl9pZFwiOkRpc3BzRmFjdG9yeS5lZGl0Ll9pZH0sXG4gICAgICAgICAgICAgICAgZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgJGxvY2F0aW9uLnBhdGgoJy8nKTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9XG4gICAgfTtcbn0pOyIsImFuZ3VsYXIubW9kdWxlKCdSZXNlcnZhc09ubGluZScpLmNvbnRyb2xsZXIoJ05vdURpc3BDb250cm9sbGVyJyxmdW5jdGlvbigkc2NvcGUsRGlzcHNGYWN0b3J5LCRsb2NhdGlvbikge1xudmFyIHVsdGltb21lcyA9IG51bGw7XG4gICAgRGlzcHNGYWN0b3J5LnN2ci5xdWVyeShmdW5jdGlvbihkaXNwcyl7XG4gICAgICAgICRzY29wZS5kaXNwcyA9IGRpc3BzO1xuICAgICAgICB2YXIgbiA9ICRzY29wZS5kaXNwcy5sZW5ndGggLSAxO1xuICAgICAgICB2YXIgdWx0aW1vZGlhID0gbmV3IERhdGUoJHNjb3BlLmRpc3BzW25dLmRpYSk7XG4gICAgICAgIHVsdGltb21lcyA9ICh1bHRpbW9kaWEuZ2V0TW9udGgoKSArIDEpO1xuICAgIH0pO1xuICAgICRzY29wZS5hZmVnaXJNZXMgPSBmdW5jdGlvbihkaWFzLG1lcyxhbm8pe1xuICAgICAgICBjb25zb2xlLmxvZyh1bHRpbW9tZXMpO1xuICAgICAgICBpZiAobWVzID4gdWx0aW1vbWVzKXtcbiAgICAgICAgICAgIHZhciBkaWFzID0gcGFyc2VJbnQoZGlhcyk7XG4gICAgICAgICAgICBmb3IgKHZhciBpID0gMTsgaTwgZGlhcysxO2krKyl7XG4gICAgICAgICAgICBEaXNwc0ZhY3Rvcnkuc3ZyLnNhdmUoe1xuICAgICAgICAgICAgICAgIFwiZGlhXCIgOiBtZXMrXCIvXCIraStcIi9cIithbm8sXG4gICAgICAgICAgICAgICAgIFwidHVybm9cIiA6IFt7XG4gICAgICAgICAgICAgICAgICAgXCJub21icmVcIiA6IFwiTWHDsWFuYVwiLFxuICAgICAgICAgICAgICAgICAgIFwicGxhemFzXCIgOiA5MFxuICAgICAgICAgICAgICAgICB9LHtcbiAgICAgICAgICAgICAgICAgICBcIm5vbWJyZVwiIDogXCJNZWRpb0RpYVwiLFxuICAgICAgICAgICAgICAgICAgIFwicGxhemFzXCIgOiA5MCAgXG4gICAgICAgICAgICAgICAgIH0se1xuICAgICAgICAgICAgICAgICAgIFwibm9tYnJlXCIgOiBcIk5vY2hlXCIsXG4gICAgICAgICAgICAgICAgICAgXCJwbGF6YXNcIiA6IDkwICAgXG4gICAgICAgICAgICAgICAgIH1dXG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAkbG9jYXRpb24ucGF0aCgnL2FkbWluaXN0cmFjaW9uJyk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcIk5PIFBVRURFUyBFTlRSQVIgRVNURSBNRVMhXCIpO1xuICAgICAgICB9XG4gICAgfTtcbn0pOyIsImFuZ3VsYXIubW9kdWxlKCdSZXNlcnZhc09ubGluZScpLmNvbnRyb2xsZXIoXCJtb3N0cmFySW1hdGdlc0N0cmxcIixmdW5jdGlvbigkc2NvcGUsaW1hdGdlc1NlcnZpY2UpIHtcbiAgICBpbWF0Z2VzU2VydmljZS5mZXRjaCgpXG4gICAgICAgIC5zdWNjZXNzKGZ1bmN0aW9uKGltYXRnZXMpe1xuICAgICAgICAgICAgJHNjb3BlLmltYXRnZXNtZW51cyA9IGltYXRnZXM7XG4gICAgICAgIH0pO1xuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgICRzY29wZS5lbGltaW5hckltYXRnZSA9IGZ1bmN0aW9uIChpbWF0Z2UpIHtcbiAgICAgICAgICAgIGltYXRnZXNTZXJ2aWNlLmRlbGV0ZShpbWF0Z2UpO1xuICAgICAgICAgICAgJHNjb3BlLmltYXRnZXMuc3BsaWNlKCRzY29wZS5pbWF0Z2VzLmluZGV4T2YoaW1hdGdlKSwxKTtcbiAgICAgICAgfSBcbiAgICAgICAgXG59KTsiLCJhbmd1bGFyLm1vZHVsZSgnUmVzZXJ2YXNPbmxpbmUnKS5jb250cm9sbGVyKFwiU3ViaXJJbWF0Z2VDdHJsXCIsIGZ1bmN0aW9uKCRzY29wZSxGaWxlVXBsb2FkZXIpIHtcbiAgICB2YXIgdXBsb2FkZXIgPSAkc2NvcGUudXBsb2FkZXIgPSBuZXcgRmlsZVVwbG9hZGVyKHt1cmw6XCIvYXBpL2ltYXRnZXNcIixhbGlhczpcImltYWdlXCIscmVtb3ZlQWZ0ZXJVcGxvYWQ6IHRydWV9KTtcbiAgICAvLyRzY29wZS51cGxvYWRlciA9ICRmaWxlVXBsb2FkZXIuY3JlYXRlKHt1cmw6XCIvYXBpL2ltYXRnZXNcIixhbGlhczpcImltYWdlXCIscmVtb3ZlQWZ0ZXJVcGxvYWQ6IHRydWV9KTtcbiAgIC8qIHVwbG9hZGVyLmJpbmQoJ2JlZm9yZXVwbG9hZCcsIGZ1bmN0aW9uIChldmVudCwgaXRlbSkge1xuICAgICAgICAgICAgaXRlbS5mb3JtRGF0YS5wdXNoKHtzb21lOiAnZGF0YSd9KTtcbiAgICAgICAgfSk7Ki9cbiAgICB1cGxvYWRlci5vbkJlZm9yZVVwbG9hZEl0ZW0gPSBmdW5jdGlvbiAoaXRlbSkge1xuICAgICAgICBpdGVtLmZvcm1EYXRhLnB1c2goe3RpdG9sOiAkc2NvcGUudGl0b2x9KTtcbiAgICAgICAgaXRlbS5mb3JtRGF0YS5wdXNoKHtkZXNjcmlwY2lvOiAkc2NvcGUuZGVzY3JpcGNpb30pO1xuICAgICAgICB9ICAgO1xufSk7IiwiYW5ndWxhci5tb2R1bGUoJ1Jlc2VydmFzT25saW5lJylcbiAgICAuc2VydmljZShcImltYXRnZXNTZXJ2aWNlTWVudXNcIiwgZnVuY3Rpb24oJGh0dHApIHtcbiAgICAgICAgdGhpcy5mZXRjaCA9IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgcmV0dXJuICRodHRwLmdldChcIi9hcGkvaW1hZ2VuZXNtZW51c1wiKTtcbiAgICAgICAgfTtcbiAgICAgICAgXG4gICAgICAgIHRoaXMuZGVsZXRlID0gZnVuY3Rpb24oaW1hdGdlKSB7XG4gICAgICAgICAgICBjb25zb2xlLmxvZyhpbWF0Z2UuX2lkKTtcbiAgICAgICAgICAgIHJldHVybiAkaHR0cC5kZWxldGUoXCIvYXBpL2ltYWdlbmVzbWVudXMvXCIraW1hdGdlLl9pZCk7XG4gICAgICAgIH07XG4gICAgfSk7IiwiYW5ndWxhci5tb2R1bGUoJ1Jlc2VydmFzT25saW5lJylcbiAgICAuc2VydmljZShcImltYXRnZXNTZXJ2aWNlXCIsIGZ1bmN0aW9uKCRodHRwKSB7XG4gICAgICAgIHRoaXMuZmV0Y2ggPSBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHJldHVybiAkaHR0cC5nZXQoXCIvYXBpL2ltYXRnZXNcIik7XG4gICAgICAgIH07XG4gICAgICAgIFxuICAgICAgICB0aGlzLmRlbGV0ZSA9IGZ1bmN0aW9uKGltYXRnZSkge1xuICAgICAgICAgICAgY29uc29sZS5sb2coaW1hdGdlLl9pZCk7XG4gICAgICAgICAgICByZXR1cm4gJGh0dHAuZGVsZXRlKFwiL2FwaS9pbWF0Z2VzL1wiK2ltYXRnZS5faWQpO1xuICAgICAgICB9O1xuICAgIH0pOyIsImFuZ3VsYXIubW9kdWxlKCdSZXNlcnZhc09ubGluZScpXG4uZmlsdGVyKFwibmwyYnJcIiwgZnVuY3Rpb24oJGZpbHRlcikge1xuIHJldHVybiBmdW5jdGlvbihkYXRhKSB7XG4gICBpZiAoIWRhdGEpIHJldHVybiBkYXRhO1xuICAgcmV0dXJuIGRhdGEucmVwbGFjZSgvXFxuXFxyPy9nLCAnPGJyIC8+Jyk7XG4gfTtcbn0pXG4gICAgLmNvbnRyb2xsZXIoXCJBcHBsaWNhdGlvbkNvbnRyb2xsZXJcIiwgZnVuY3Rpb24oJGludGVydmFsLCRzY29wZSwkbG9jYXRpb24sVXNlclN2YywkY29va2llcyxpbWF0Z2VzU2VydmljZSxJbmljaW9GYWN0b3J5KSB7XG4gICAgSW5pY2lvRmFjdG9yeS5zdnIucXVlcnkoZnVuY3Rpb24oaW5pY2lvKXtcbiAgICAgICAgJHNjb3BlLmluaWNpbyA9IGluaWNpbztcbiAgICB9KTtcbiAgICBcbiAgICAkc2NvcGUuTW9kaWZpY2FySW5mb3JtYWNpb24gPSBmdW5jdGlvbigpe1xuICAgICAgICAgICAgSW5pY2lvRmFjdG9yeS5zdnIudXBkYXRlKHtcIl9pZFwiOiRzY29wZS5pbmljaW9bMF0uX2lkLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwibm9tYnJlXCI6ICAkc2NvcGUuaW5pY2lvWzBdLm5vbWJyZSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcImluZm9ybWFjaW9uXCI6ICRzY29wZS5pbmljaW9bMF0uaW5mb3JtYWNpb25cbiAgICAgICAgICAgIH0pOyAgICAgICAgXG4gICAgfTtcbiAgICAgICAgJHNjb3BlLiRvbignbG9naW4nLCBmdW5jdGlvbihlLHVzZXIpIHtcbiAgICAgICAgICAgICRzY29wZS5jdXJyZW50VXNlciA9IHVzZXI7XG4gICAgICAgIH0pO1xuICAgICAgICAkc2NvcGUubG9nb3V0ID0gZnVuY3Rpb24oKXtcbiAgICAgICAgICAgIFVzZXJTdmMubG9nT3V0KCk7XG4gICAgICAgICAgICBkZWxldGUgJHNjb3BlLmN1cnJlbnRVc2VyO1xuICAgICAgICAgICAgJGxvY2F0aW9uLnBhdGgoJy8nKTtcbiAgICAgICAgfTtcblxuXG4gICAgaW1hdGdlc1NlcnZpY2UuZmV0Y2goKVxuICAgICAgICAuc3VjY2VzcyhmdW5jdGlvbihpbWF0Z2VzKXtcbiAgICAgICAgICAgICRzY29wZS5pbWF0Z2VzID0gaW1hdGdlcztcbiAgICAgICAgfSk7XG4gICAgICAgIFxuICAgICAgICBcbiAgICAkc2NvcGUubGF5b3V0RG9uZSA9IGZ1bmN0aW9uKCkge1xuICAgICAgICAkKCcuc2xpZGVyJykuc2xpZGVyKCk7XG4gICAgfTtcbiAgICBcblxuICAgIH0pOyIsIi8vIC0tLS0gUkVTT1VSQ0UgQU5HVUxBUiAtLS0tLS0tXG5hbmd1bGFyLm1vZHVsZSgnUmVzZXJ2YXNPbmxpbmUnKS5mYWN0b3J5KFwiSW5pY2lvRmFjdG9yeVwiLCBmdW5jdGlvbigkcmVzb3VyY2UpIHtcbiAgICByZXR1cm4ge1xuICAgICAgICBzdnIgOiAkcmVzb3VyY2UoXCIvYXBpL2luaWNpLzppZFwiLCBudWxsLFxuICAgICAgICB7XG4gICAgICAgICAgICAndXBkYXRlJzogeyBtZXRob2Q6J1BVVCcgfVxuICAgICAgICB9KVxuICAgICAgICAsXG4gICAgICAgIGVkaXQgOiBudWxsXG4gICAgfVxuICAgIFxufSk7IiwiYW5ndWxhci5tb2R1bGUoJ1Jlc2VydmFzT25saW5lJykuY29udHJvbGxlcignRWRpdGFyTWVudUNvbnRyb2xsZXInLGZ1bmN0aW9uKCRzY29wZSwkbG9jYXRpb24sTWVudXNGYWN0b3J5KSB7XG4gICAgICAgICAgICAkc2NvcGUuZWRpdE5vbWJyZSA9IE1lbnVzRmFjdG9yeS5lZGl0Lm5vbWJyZTtcbiAgICAgICAgICAgICRzY29wZS5lZGl0UHJpbWVyb3MgPSBNZW51c0ZhY3RvcnkuZWRpdC5wcmltZXJvcztcbiAgICAgICAgICAgICRzY29wZS5lZGl0U2VndW5kb3MgPSBNZW51c0ZhY3RvcnkuZWRpdC5zZWd1bmRvcztcbiAgICAgICAgICAgICRzY29wZS5lZGl0UHJlY2lvID0gTWVudXNGYWN0b3J5LmVkaXQucHJlY2lvO1xuXG4kc2NvcGUudXBkYXRlTWVudSA9IGZ1bmN0aW9uKCkge1xuICAgICAgICBpZiAoJHNjb3BlLmVkaXROb21icmUgJiYgJHNjb3BlLmVkaXRQcmltZXJvcyAmJiAkc2NvcGUuZWRpdFNlZ3VuZG9zIFxuICAgICAgICAmJiAkc2NvcGUuZWRpdFByZWNpbykge1xuICAgICAgICAgICAgTWVudXNGYWN0b3J5LnN2ci51cGRhdGUoe1wiX2lkXCI6TWVudXNGYWN0b3J5LmVkaXQuX2lkLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwibm9tYnJlXCI6ICAkc2NvcGUuZWRpdE5vbWJyZSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcInByaW1lcm9zXCI6ICRzY29wZS5lZGl0UHJpbWVyb3MsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJzZWd1bmRvc1wiOiAkc2NvcGUuZWRpdFNlZ3VuZG9zLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwicHJlY2lvXCI6ICRzY29wZS5lZGl0UHJlY2lvXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgICRsb2NhdGlvbi5wYXRoKCcvbWVudXMnKTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9XG4gICAgfTtcbn0pOyIsImFuZ3VsYXIubW9kdWxlKCdSZXNlcnZhc09ubGluZScpLmNvbnRyb2xsZXIoJ01lbnVzQ29udHJvbGxlcicsZnVuY3Rpb24oJHNjb3BlLCRsb2NhdGlvbixNZW51c0ZhY3RvcnksaW1hdGdlc1NlcnZpY2VNZW51cykge1xuICAgICRzY29wZS5tZW51cyA9IFtdO1xuXG4gICAgLy8gLS0tLSBHRVQgLS0tLS1cbiAgICBpbWF0Z2VzU2VydmljZU1lbnVzLmZldGNoKClcbiAgICAgICAgLnN1Y2Nlc3MoZnVuY3Rpb24oaW1hdGdlcyl7XG4gICAgICAgICAgICAkc2NvcGUuaW1hdGdlcyA9IGltYXRnZXM7XG4gICAgfSk7XG5cblxuICAgIE1lbnVzRmFjdG9yeS5zdnIucXVlcnkoZnVuY3Rpb24obWVudXMpe1xuICAgICAgICAkc2NvcGUubWVudXMgPSBtZW51cztcbiAgICB9KTtcblxuICAgICRzY29wZS5lZGl0YXJNZW51ID0gZnVuY3Rpb24obWVudSkge1xuICAgICAgICBNZW51c0ZhY3RvcnkuZWRpdCA9IG1lbnU7XG4gICAgICAgICRsb2NhdGlvbi5wYXRoKCcvZWRpdGFybWVudScpO1xuICAgIH07XG5cbiAgICAkc2NvcGUuZWxpbWluYXJNZW51ID0gZnVuY3Rpb24obWVudSkge1xuICAgICAgICBjb25zb2xlLmxvZyhtZW51Ll9pZCk7XG4gICAgICAgIE1lbnVzRmFjdG9yeS5zdnIuZGVsZXRlKHtpZDptZW51Ll9pZH0sZnVuY3Rpb24obCl7XG4gICAgICAgICAgICBjb25zb2xlLmxvZygkc2NvcGUubWVudXMuaW5kZXhPZihtZW51KSk7XG4gICAgICAgICAgICAkc2NvcGUubWVudXMuc3BsaWNlKCRzY29wZS5tZW51cy5pbmRleE9mKG1lbnUpLDEpO1xuICAgICAgICB9KTtcbiAgICAgICAgY29uc29sZS5sb2cobWVudSk7XG4gICAgfTtcbn0pOyIsIi8vIC0tLS0gUkVTT1VSQ0UgQU5HVUxBUiAtLS0tLS0tXG5hbmd1bGFyLm1vZHVsZSgnUmVzZXJ2YXNPbmxpbmUnKS5mYWN0b3J5KFwiTWVudXNGYWN0b3J5XCIsIGZ1bmN0aW9uKCRyZXNvdXJjZSkge1xuICAgIHJldHVybiB7XG4gICAgICAgIHN2ciA6ICRyZXNvdXJjZShcIi9hcGkvbWVudS86aWRcIiwgbnVsbCxcbiAgICAgICAge1xuICAgICAgICAgICAgJ3VwZGF0ZSc6IHsgbWV0aG9kOidQVVQnIH1cbiAgICAgICAgfSlcbiAgICAgICAgLFxuICAgICAgICBlZGl0IDogbnVsbFxuICAgIH1cbiAgICBcbn0pOyIsImFuZ3VsYXIubW9kdWxlKCdSZXNlcnZhc09ubGluZScpLmNvbnRyb2xsZXIoJ05vdU1lbnVDb250cm9sbGVyJyxmdW5jdGlvbigkc2NvcGUsTWVudXNGYWN0b3J5LCAkbG9jYXRpb24sRmlsZVVwbG9hZGVyKSB7XG4gICAgdmFyIHVwbG9hZGVyID0gJHNjb3BlLnVwbG9hZGVyID0gbmV3IEZpbGVVcGxvYWRlcih7dXJsOlwiL2FwaS9tZW51XCIsYWxpYXM6XCJmb3RvXCIscmVtb3ZlQWZ0ZXJVcGxvYWQ6IHRydWV9KTtcbiAgICAgICAgdXBsb2FkZXIub25CZWZvcmVVcGxvYWRJdGVtID0gZnVuY3Rpb24gKGl0ZW0pIHtcbiAgICAgICAgICAgIGl0ZW0uZm9ybURhdGEucHVzaCh7dGl0b2w6IFwiXCJ9KTtcbiAgICAgICAgICAgIGl0ZW0uZm9ybURhdGEucHVzaCh7ZGVzY3JpcGNpb246IFwiXCJ9KTtcbiAgICAgICAgICAgIGl0ZW0uZm9ybURhdGEucHVzaCh7bm9tYnJlOiAkc2NvcGUubm9tYnJlfSk7XG4gICAgICAgICAgICBpdGVtLmZvcm1EYXRhLnB1c2goe3ByaW1lcm9zOiAkc2NvcGUucHJpbWVyb3N9KTtcbiAgICAgICAgICAgIGl0ZW0uZm9ybURhdGEucHVzaCh7c2VndW5kb3M6ICRzY29wZS5zZWd1bmRvc30pO1xuICAgICAgICAgICAgaXRlbS5mb3JtRGF0YS5wdXNoKHtwcmVjaW86ICRzY29wZS5wcmVjaW99KTtcbiAgICAgICAgfTtcbiAgICAgICAgXG4gICAgICAgICRzY29wZS5hZmVnaXJNZW51ID0gZnVuY3Rpb24oKSB7XG4gICAgICAgIGlmICgkc2NvcGUuTm9tYnJlICYmICRzY29wZS5QcmltZXJvcyAmJiAkc2NvcGUuU2VndW5kb3MgXG4gICAgICAgICYmICRzY29wZS5QcmVjaW8pIHtcbiAgICAgICAgICAgIE1lbnVzRmFjdG9yeS5zdnIuc2F2ZSh7XG4gICAgICAgICAgICAgICAgbm9tYnJlIDogJHNjb3BlLk5vbWJyZSxcbiAgICAgICAgICAgICAgICBwcmltZXJvczogJHNjb3BlLlByaW1lcm9zLFxuICAgICAgICAgICAgICAgIHNlZ3VuZG9zOiAkc2NvcGUuU2VndW5kb3MsXG4gICAgICAgICAgICAgICAgcHJlY2lvOiAkc2NvcGUuUHJlY2lvLFxuICAgICAgICAgICAgICAgIGZvdG8gOiAkc2NvcGUuZm90b1xuICAgICAgICAgICAgfSwgZnVuY3Rpb24oYXV0b3IpIHtcbiAgICAgICAgICAgICAgICAkc2NvcGUuTm9tYnJlID0gbnVsbDtcbiAgICAgICAgICAgICAgICAkc2NvcGUuUHJpbWVyb3MgPSBudWxsO1xuICAgICAgICAgICAgICAgICRzY29wZS5TZWd1bmRvcyA9IG51bGw7XG4gICAgICAgICAgICAgICAgJHNjb3BlLlByZWNpbyA9IG51bGw7XG4gICAgICAgICAgICAgICAgJHNjb3BlLmZvdG8gPSBudWxsO1xuICAgICAgICAgICAgICAgICRsb2NhdGlvbi5wYXRoKCcvbWVudXMnKTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9XG4gICAgfTtcbn0pOyIsImFuZ3VsYXIubW9kdWxlKCdSZXNlcnZhc09ubGluZScpLmNvbnRyb2xsZXIoJ0VkaXRhclJlc2VydmFDb250cm9sbGVyJyxmdW5jdGlvbigkc2NvcGUsJGludGVydmFsLCRsb2NhdGlvbixSZXNlcnZhc0ZhY3RvcnksIE1lbnVzRmFjdG9yeSwgRGlzcHNGYWN0b3J5KSB7XG4kc2NvcGUuZWRpdE5vbWJyZSA9IFJlc2VydmFzRmFjdG9yeS5lZGl0Lm5vbWJyZTtcbiRzY29wZS5lZGl0Q29udGFjdG8gPSBSZXNlcnZhc0ZhY3RvcnkuZWRpdC5jb250YWN0bztcbiRzY29wZS5lZGl0TnVtUGVyc29uYXMgPSBSZXNlcnZhc0ZhY3RvcnkuZWRpdC5udW1wZXJzb25hcztcbiRzY29wZS5lZGl0SW1wb3J0ZSA9IFJlc2VydmFzRmFjdG9yeS5lZGl0LmltcG9ydGU7XG4kc2NvcGUubWVudXMgPSBbXTtcbiRzY29wZS5kaXNwcyA9IFtdO1xuJHNjb3BlLnR1cm5vcyA9IFtdO1xudmFyIEFudGRpYSA9IFJlc2VydmFzRmFjdG9yeS5lZGl0LmRpYTtcbnZhciBBbnRUdXJub19pZCA9IFJlc2VydmFzRmFjdG9yeS5lZGl0LnR1cm5vX2lkO1xudmFyIEFudE51bVBlcnNvbmFzID0gUmVzZXJ2YXNGYWN0b3J5LmVkaXQubnVtcGVyc29uYXM7XG52YXIgZGlhID0gbnVsbDtcbnZhciBlbnRyYWRvID0gbnVsbDtcbnZhciB0dXJubyA9IG51bGw7XG52YXIgYWxnbyA9IHRydWU7XG4gICAgICAgIHZhciBzdG9wO1xuICAgICAgICAgIHN0b3AgPSAkaW50ZXJ2YWwoZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgIGlmIChhbGdvKXtcbiAgICAgICAgICAgICAgICAgIGFsZ28gPSBmYWxzZTtcbiAgICAgICAgICAgICAgICAkKCdzZWxlY3QnKS5tYXRlcmlhbF9zZWxlY3QoKTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgIH0sIDUwMCk7XG5cblxuXG4gICAgICAgIE1lbnVzRmFjdG9yeS5zdnIucXVlcnkoZnVuY3Rpb24obWVudXMpe1xuICAgICAgICAgICAgJHNjb3BlLm1lbnVzID0gbWVudXM7XG4gICAgICAgIH0pO1xuICAgICAgICBEaXNwc0ZhY3Rvcnkuc3ZyLnF1ZXJ5KGZ1bmN0aW9uKGRpc3BzKXtcbiAgICAgICAgICAgICRzY29wZS5kaXNwcyA9IGRpc3BzO1xuICAgICAgICB9KTtcbiAgICAgICAgJHNjb3BlLiR3YXRjaCgnZWRpdE1lbnUnLCBmdW5jdGlvbihuZXdWYWwpIHtcbiAgICAgICAgICAgIGlmICgkc2NvcGUuZWRpdE1lbnUgJiYgJHNjb3BlLmVkaXROdW1QZXJzb25hcyl7XG4gICAgICAgICAgICAkc2NvcGUubWVudXMuZm9yRWFjaChmdW5jdGlvbihlLGkpe1xuICAgICAgICAgICAgICAgIGlmIChlLl9pZCA9PSAkc2NvcGUuZWRpdE1lbnUuX2lkKSB7XG4gICAgICAgICAgICAgICAgICAgICRzY29wZS5lZGl0SW1wb3J0ZSA9IGUucHJlY2lvICogJHNjb3BlLmVkaXROdW1QZXJzb25hcztcbiAgICAgICAgICAgICAgICB9ICAgIFxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgJHNjb3BlLmVkaXRJbXBvcnRlID0gMDsgXG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgICAgICAkc2NvcGUuJG9uKCdDb2dlckRpYScsIGZ1bmN0aW9uKCl7XG4gICAgICAgICAgICBlbnRyYWRvID0gUmVzZXJ2YXNGYWN0b3J5LmRpYTtcbiAgICAgICAgICAgICRzY29wZS5kaXNwcy5mb3JFYWNoKGZ1bmN0aW9uKGUsaSl7XG4gICAgICAgICAgICAgICAgdmFyIG51ZXZvID0gbmV3IERhdGUoRGF0ZS5wYXJzZShlLmRpYSkpO1xuICAgICAgICAgICAgICAgIGRpYSA9IChudWV2by5nZXREYXRlKCkrXCIvXCIrKG51ZXZvLmdldE1vbnRoKCkrMSkrXCIvXCIrbnVldm8uZ2V0RnVsbFllYXIoKSk7XG4gICAgICAgICAgICAgICAgaWYgKGRpYSA9PSBlbnRyYWRvKXtcbiAgICAgICAgICAgICAgICAgICAgJHNjb3BlLnR1cm5vcyA9IGUudHVybm87XG4gICAgICAgICAgICAgICAgICAgICRzY29wZS5lZGl0RGlhID0gZS5faWQ7XG4gICAgICAgICAgICAgICAgICAgICRzY29wZS4kYXBwbHkoKTtcbiAgICAgICAgICAgICAgICAgICAgJCgnc2VsZWN0JykubWF0ZXJpYWxfc2VsZWN0KCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0pO1xuICAgICAgICAkc2NvcGUuJHdhdGNoKCdlZGl0VHVybm8nLCBmdW5jdGlvbihuZXdWYWwpIHtcbiAgICAgICAgICAgICRzY29wZS50dXJub3MuZm9yRWFjaChmdW5jdGlvbihlLGkpe1xuICAgICAgICAgICAgICAgIGlmIChlLl9pZCA9PSAkc2NvcGUuZWRpdFR1cm5vLl9pZCl7XG4gICAgICAgICAgICAgICAgICAgIHR1cm5vID0gZS5ub21icmU7XG4gICAgICAgICAgICAgICAgICAgICRzY29wZS5wbGF6YXMgPSBlLnBsYXphcztcbiAgICAgICAgICAgICAgICAgICAgaWYoJHNjb3BlLnBsYXphcyA8ICRzY29wZS5lZGl0TnVtUGVyc29uYXMpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICRzY29wZS5kaXNwb25pYmxlID0gdHJ1ZTtcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICRzY29wZS5kaXNwb25pYmxlID0gZmFsc2U7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSk7XG5cblxuJHNjb3BlLnVwZGF0ZVJlc2VydmEgPSBmdW5jdGlvbigpIHtcbiAgICAgICAgaWYgKCRzY29wZS5lZGl0Tm9tYnJlICYmICRzY29wZS5lZGl0Q29udGFjdG8gJiYgJHNjb3BlLmVkaXROdW1QZXJzb25hc1xuICAgICAgICAmJiAkc2NvcGUuZWRpdE1lbnUpIHtcbiAgICAgICAgICAgIFJlc2VydmFzRmFjdG9yeS5zdnIudXBkYXRlKHtcIl9pZFwiOlJlc2VydmFzRmFjdG9yeS5lZGl0Ll9pZCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcIm5vbWJyZVwiOiAgJHNjb3BlLmVkaXROb21icmUsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJjb250YWN0b1wiOiAkc2NvcGUuZWRpdENvbnRhY3RvLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwibnVtcGVyc29uYXNcIjogJHNjb3BlLmVkaXROdW1QZXJzb25hcyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcIm1lbnVcIjogJHNjb3BlLmVkaXRNZW51Ll9pZCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcImRpYVwiIDogJHNjb3BlLmVkaXREaWEsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJ0dXJub1wiIDogdHVybm8sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJ0dXJub19pZFwiIDogJHNjb3BlLmVkaXRUdXJuby5faWQsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJpbXBvcnRlXCIgOiAkc2NvcGUuZWRpdEltcG9ydGVcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgICAgIERpc3BzRmFjdG9yeS5zdnIudXBkYXRlKHtcIl9pZFwiOkFudGRpYSxcInR1cm5vX2lkXCI6QW50VHVybm9faWQsXCJwbGF6YXNcIiA6IEFudE51bVBlcnNvbmFzLFwiYWNjaW9uXCIgOiBcIm1hc1wifSk7XG4gICAgICAgICAgICAgICAgICAgIERpc3BzRmFjdG9yeS5zdnIudXBkYXRlKHtcIl9pZFwiOiRzY29wZS5lZGl0RGlhLFwidHVybm9faWRcIjokc2NvcGUuZWRpdFR1cm5vLl9pZCxcInBsYXphc1wiIDogJHNjb3BlLmVkaXROdW1QZXJzb25hcyxcImFjY2lvblwiIDogXCJtZW5vc1wifSk7XG4gICAgICAgICAgICAgICAgJGxvY2F0aW9uLnBhdGgoJy9yZXNlcnZhcycpO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICB9O1xufSk7IiwiYW5ndWxhci5tb2R1bGUoJ1Jlc2VydmFzT25saW5lJykuY29udHJvbGxlcignTm91UmVzZXJ2YUNvbnRyb2xsZXInLGZ1bmN0aW9uKGltYXRnZXNTZXJ2aWNlTWVudXMsJHJvb3RTY29wZSwkaW50ZXJ2YWwsJHNjb3BlLFJlc2VydmFzRmFjdG9yeSwkbG9jYXRpb24sIE1lbnVzRmFjdG9yeSxEaXNwc0ZhY3RvcnksIE1haWxpbmdGYWN0b3J5KSB7XG4kc2NvcGUubWVudXMgPSBbXTtcbiRzY29wZS5kaXNwcyA9IFtdO1xuJHNjb3BlLnR1cm5vcyA9IFtdO1xuJHNjb3BlLnBsYXphcztcbiRzY29wZS5kaXNwb25pYmxlID0gZmFsc2U7XG4kc2NvcGUuSW1wb3J0ZSA9IDA7XG4kc2NvcGUubW9zdHJhciA9IGZhbHNlO1xudmFyIGNvZGlnb1ZhbGlkYXIgPSBcIjEyMzRcIjtcblxudmFyIE5vZWplY3V0YWRvID0gdHJ1ZTtcbiAgICAgICAgdmFyIHN0b3A7XG4gICAgICAgICAgc3RvcCA9ICRpbnRlcnZhbChmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgaWYgKE5vZWplY3V0YWRvKXtcbiAgICAgICAgICAgICAgICAgIE5vZWplY3V0YWRvID0gZmFsc2U7XG4gICAgICAgICAgICAgICAgJCgnc2VsZWN0JykubWF0ZXJpYWxfc2VsZWN0KCk7XG4gICAgICAgICAgICAgICAgdmFyIGNhbGVuZGFyaW8gPSBSZXNlcnZhc0ZhY3RvcnkuY2FsZW5kYXJpbztcbiAgICAgICAgICAgICAgICBEaXNwc0ZhY3RvcnkuZGlzcG9uaWJsZS5xdWVyeShmdW5jdGlvbihkaXNwcyl7XG4gICAgICAgICAgICAgICAgICAgIENvbXByb3ZhckRpc3BvbmliaWxpZGFkKGRpc3BzLGNhbGVuZGFyaW8pO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgfSwgNTAwKTtcbnZhciBkaWEgPSBudWxsO1xudmFyIGVudHJhZG8gPSBudWxsO1xudmFyIHR1cm5vID0gbnVsbDtcbiAgICBpbWF0Z2VzU2VydmljZU1lbnVzLmZldGNoKClcbiAgICAgICAgLnN1Y2Nlc3MoZnVuY3Rpb24oaW1hdGdlcyl7XG4gICAgICAgICAgICAkc2NvcGUuaW1hdGdlcyA9IGltYXRnZXM7XG4gICAgfSk7XG4gICAgICAgIE1lbnVzRmFjdG9yeS5zdnIucXVlcnkoZnVuY3Rpb24obWVudXMpe1xuICAgICAgICAgICAgJHNjb3BlLm1lbnVzID0gbWVudXM7XG4gICAgICAgIH0pO1xuICAgICAgICBEaXNwc0ZhY3RvcnkuZGlzcG9uaWJsZS5xdWVyeShmdW5jdGlvbihkaXNwcyl7XG4gICAgICAgICAgICAkc2NvcGUuZGlzcHMgPSBkaXNwcztcbiAgICAgICAgfSk7XG4gICAgICAgICRzY29wZS4kd2F0Y2goJ01lbnUnLCBmdW5jdGlvbihuZXdWYWwpIHtcbiAgICAgICAgICAgIGlmICgkc2NvcGUuTWVudSAmJiAkc2NvcGUuTnVtUGVyc29uYXMpe1xuICAgICAgICAgICAgJHNjb3BlLm1lbnVzLmZvckVhY2goZnVuY3Rpb24oZSxpKXtcbiAgICAgICAgICAgICAgICBpZiAoZS5faWQgPT0gJHNjb3BlLk1lbnUuX2lkKSB7XG4gICAgICAgICAgICAgICAgICAgICRzY29wZS5JbXBvcnRlID0gZS5wcmVjaW8gKiAkc2NvcGUuTnVtUGVyc29uYXM7XG4gICAgICAgICAgICAgICAgfSAgICBcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICRzY29wZS5JbXBvcnRlID0gMDsgXG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgICAgICAkc2NvcGUuJG9uKCdDb2dlckRpYScsIGZ1bmN0aW9uKCl7XG4gICAgICAgICAgICBlbnRyYWRvID0gUmVzZXJ2YXNGYWN0b3J5LmRpYTtcbiAgICAgICAgICAgICRzY29wZS5lbmNvbnRyYWRvID0gZmFsc2U7XG4gICAgICAgICAgICAkc2NvcGUuZGlzcHMuZm9yRWFjaChmdW5jdGlvbihlLGkpe1xuICAgICAgICAgICAgICAgIHZhciBudWV2byA9IG5ldyBEYXRlKERhdGUucGFyc2UoZS5kaWEpKTtcbiAgICAgICAgICAgICAgICBkaWEgPSAobnVldm8uZ2V0RGF0ZSgpK1wiL1wiKyhudWV2by5nZXRNb250aCgpKzEpK1wiL1wiK251ZXZvLmdldEZ1bGxZZWFyKCkpO1xuICAgICAgICAgICAgICAgIGlmIChkaWEgPT0gZW50cmFkbyl7XG4gICAgICAgICAgICAgICAgICAgICRzY29wZS5UdXJubyA9IG51bGw7XG4gICAgICAgICAgICAgICAgICAgICRzY29wZS50dXJub3MgPSBlLnR1cm5vO1xuICAgICAgICAgICAgICAgICAgICAkc2NvcGUuRGlhID0gZS5faWQ7XG4gICAgICAgICAgICAgICAgICAgICRzY29wZS5lbmNvbnRyYWRvID0gdHJ1ZTtcbiAgICAgICAgICAgICAgICAgICAgJHNjb3BlLm1vc3RyYXIgPSB0cnVlO1xuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcIm1vc3RyYXI6IFwiKyRzY29wZS5tb3N0cmFyKTtcbiAgICAgICAgICAgICAgICAgICAgJHNjb3BlLiRhcHBseSgpO1xuICAgICAgICAgICAgICAgICAgICAkKCdzZWxlY3QnKS5tYXRlcmlhbF9zZWxlY3QoKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIGlmICghJHNjb3BlLmVuY29udHJhZG8pe1xuICAgICAgICAgICAgICAgICRzY29wZS50dXJub3MgPSBbXTtcbiAgICAgICAgICAgICAgICAkc2NvcGUubW9zdHJhciA9IGZhbHNlO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICAgICAgdmFyIGNvcnJlY3RvO1xuICAgICAgICAkc2NvcGUuJHdhdGNoKCdOdW1QZXJzb25hcycsZnVuY3Rpb24obmV3VmFsKSAgIHtcbiAgICAgICAgICAgIGlmICghaXNOYU4oJHNjb3BlLk51bVBlcnNvbmFzKSl7XG4gICAgICAgICAgICAgICAgJHNjb3BlLmNvcnJlY3RvID0gdHJ1ZTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgJHNjb3BlLmNvcnJlY3RvID0gZmFsc2U7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pXG4gICAgICAgICRzY29wZS4kd2F0Y2goJ1R1cm5vJywgZnVuY3Rpb24obmV3VmFsKSB7XG4gICAgICAgICAgICAkc2NvcGUudHVybm9zLmZvckVhY2goZnVuY3Rpb24oZSxpKXtcbiAgICAgICAgICAgICAgICBpZiAoZS5faWQgPT0gJHNjb3BlLlR1cm5vLl9pZCl7XG4gICAgICAgICAgICAgICAgICAgIHR1cm5vID0gZS5ub21icmU7XG4gICAgICAgICAgICAgICAgICAgICRzY29wZS5wbGF6YXMgPSBlLnBsYXphcztcbiAgICAgICAgICAgICAgICAgICAgaWYoJHNjb3BlLnBsYXphcyA8ICRzY29wZS5OdW1QZXJzb25hcykge1xuICAgICAgICAgICAgICAgICAgICAgICAgJHNjb3BlLmRpc3BvbmlibGUgPSB0cnVlO1xuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgJHNjb3BlLmRpc3BvbmlibGUgPSBmYWxzZTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9KTtcblxuICAgICAgICAkc2NvcGUuYWZlZ2lyUmVzZXJ2YSA9IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgY29uc29sZS5sb2cgKGNvZGlnb1ZhbGlkYXIgPT0gJHNjb3BlLkNvZGlnbyk7XG4gICAgICAgICAgICBpZiAoY29kaWdvVmFsaWRhciA9PSAkc2NvcGUuQ29kaWdvIHx8ICRzY29wZS5jdXJyZW50VXNlcil7XG4gICAgICAgICAgICAgICAgXG5cbiAgICAgICAgaWYgKCRzY29wZS5Ob21icmUgJiYgJHNjb3BlLkNvbnRhY3RvICYmICRzY29wZS5jb3JyZWN0b1xuICAgICAgICAmJiAkc2NvcGUuTWVudSAmJiAkc2NvcGUuRGlhICkge1xuICAgICAgICAgICAgUmVzZXJ2YXNGYWN0b3J5LnN2ci5zYXZlKHtcbiAgICAgICAgICAgICAgICBub21icmUgOiAkc2NvcGUuTm9tYnJlLFxuICAgICAgICAgICAgICAgIGNvbnRhY3RvOiAkc2NvcGUuQ29udGFjdG8sXG4gICAgICAgICAgICAgICAgbnVtcGVyc29uYXMgOiAkc2NvcGUuTnVtUGVyc29uYXMsXG4gICAgICAgICAgICAgICAgbWVudSA6ICRzY29wZS5NZW51Ll9pZCxcbiAgICAgICAgICAgICAgICBkaWEgOiAkc2NvcGUuRGlhLFxuICAgICAgICAgICAgICAgIHR1cm5vIDogdHVybm8sXG4gICAgICAgICAgICAgICAgdHVybm9faWQgOiAkc2NvcGUuVHVybm8uX2lkLFxuICAgICAgICAgICAgICAgIGltcG9ydGU6ICRzY29wZS5JbXBvcnRlLFxuICAgICAgICAgICAgfSwgZnVuY3Rpb24ocmVzZXJ2YSkge1xuICAgICAgICAgICAgICAgIERpc3BzRmFjdG9yeS5zdnIudXBkYXRlKHtcIl9pZFwiOiRzY29wZS5EaWEsXCJ0dXJub19pZFwiOiRzY29wZS5UdXJuby5faWQsXCJwbGF6YXNcIiA6ICRzY29wZS5OdW1QZXJzb25hcyxcImFjY2lvblwiIDogXCJtZW5vc1wifSk7XG4gICAgICAgICAgICAgICAgJHNjb3BlLk5vbWJyZSA9IG51bGw7XG4gICAgICAgICAgICAgICAgJHNjb3BlLkNvbnRhY3RvID0gbnVsbDtcbiAgICAgICAgICAgICAgICAkc2NvcGUuTnVtUGVyc29uYXMgPSBudWxsO1xuICAgICAgICAgICAgICAgICRzY29wZS5NZW51ID0gbnVsbDtcbiAgICAgICAgICAgICAgICAkc2NvcGUuRGlhID0gbnVsbDtcbiAgICAgICAgICAgICAgICB0dXJubyA9IG51bGw7XG4gICAgICAgICAgICAgICAgJHNjb3BlLlR1cm5vID0gbnVsbDtcbiAgICAgICAgICAgICAgICAkc2NvcGUuSW1wb3J0ZSA9IG51bGw7XG4gICAgICAgICAgICAgICAgaWYgKCRzY29wZS5jdXJyZW50VXNlcikge1xuICAgICAgICAgICAgICAgICAgICAkbG9jYXRpb24ucGF0aCgnL3Jlc2VydmFzJyk7XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgJGxvY2F0aW9uLnBhdGgoJy8nKTtcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9IGVsc2UgeyAgY29uc29sZS5sb2coXCJEQVRPUyBJTkNPUlJFQ1RPIE8gSU5DT01QTEVUT1NcIilcbiAgICAgICAgICAgIH1cbiAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiQ09ESUdPIElOQ09SUkVDVE9cIik7XG4gICAgICAgICAgICB9XG4gICAgfTtcbiAgICBcbiAgICBmdW5jdGlvbiBDb21wcm92YXJEaXNwb25pYmlsaWRhZChkaXNwb25pYmxlcyxjYWxlbmRhcmlvKXtcbiAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBjYWxlbmRhcmlvLmxlbmd0aDsgaSsrKXsgICAgICAgIFxuICAgICAgICAgICAgdmFyIGRpc3BvbmlibGUgPSBmYWxzZTtcbiAgICAgICAgdmFyIGMgPSBjYWxlbmRhcmlvW2ldO1xuICAgICAgICB2YXIgZGlhYyA9ICQoYykudGV4dCgpKyBcIi9cIitnX2NhbGVuZGFyT2JqZWN0WydjdXJyZW50TW9udGgnXStcIi9cIitnX2NhbGVuZGFyT2JqZWN0WydjdXJyZW50WWVhciddO1xuICAgICAgICBkaXNwb25pYmxlcy5mb3JFYWNoKGZ1bmN0aW9uKGUsaSl7XG4gICAgICAgICAgICB2YXIgbnVldm8gPSBuZXcgRGF0ZShEYXRlLnBhcnNlKGUuZGlhKSk7XG4gICAgICAgICAgICBkaWEgPSAobnVldm8uZ2V0RGF0ZSgpK1wiL1wiKyhudWV2by5nZXRNb250aCgpKzEpK1wiL1wiK251ZXZvLmdldEZ1bGxZZWFyKCkpO1xuICAgICAgICAgICAgaWYgKGRpYSA9PSBkaWFjKSB7XG4gICAgICAgICAgICAgICAgZGlzcG9uaWJsZSA9IHRydWU7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgICAgICBpZiAoZGlzcG9uaWJsZSl7XG4gICAgICAgICAgICAkKGMpLmF0dHIoXCJkaXNwb25pYmxlXCIsdHJ1ZSk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAkKGMpLmF0dHIoXCJkaXNwb25pYmxlXCIsZmFsc2UpO1xuICAgICAgICAgICAgJChjKS5jc3MoXCJiYWNrZ3JvdW5kXCIsXCJ1cmwoaHR0cHM6Ly9wcm9qZWN0b2ZpbmFsLXJyb2RyaWd1ZXoyMy5jOS5pby9pbWcvb3JhbmdlX2RheU5vcm1hbC5naWYpIDAlIDAlIG5vLXJlcGVhdFwiKTsgXG4gICAgICAgIH1cbiAgICAgICAgLy9jb25zb2xlLmxvZyhkaXNwb25pYmxlKTtcbiAgICAgICAgfVxuICAgIH1cbiAgICBcbiAgICBcbiAgICAkc2NvcGUuRW52aWFyQ29kaWdvID0gZnVuY3Rpb24oKSB7XG4gICAgICAgIGNvZGlnb1ZhbGlkYXIgPSAneHh4eHh4eHgteHh4eC00eHh4LXl4eHgteHh4eHh4eHh4eHh4Jy5yZXBsYWNlKC9beHldL2csIGZ1bmN0aW9uKGMpIHsgXG4gICAgICAgICAgICB2YXIgciA9IE1hdGgucmFuZG9tKCkqMTZ8MCwgdiA9IGMgPT0gJ3gnID8gciA6IChyJjB4M3wweDgpOyByZXR1cm4gdi50b1N0cmluZygxNik7XG4gICAgICAgIH0pO1xuXG4gICAgICAgIE1haWxpbmdGYWN0b3J5LnN2ci5zYXZlKHtcbiAgICAgICAgICAgIFwiZW1haWxcIiA6ICRzY29wZS5FbWFpbCxcbiAgICAgICAgICAgIFwiY29kaWdvXCIgOiBjb2RpZ29WYWxpZGFyXG4gICAgICAgIH0pO1xuICAgIH1cbn0pOyIsImFuZ3VsYXIubW9kdWxlKCdSZXNlcnZhc09ubGluZScpLmNvbnRyb2xsZXIoJ1Jlc2VydmFzQ29udHJvbGxlcicsZnVuY3Rpb24oJGludGVydmFsLCRzY29wZSwkbG9jYXRpb24sUmVzZXJ2YXNGYWN0b3J5LCBNZW51c0ZhY3RvcnksRGlzcHNGYWN0b3J5KSB7XG4gICAgJHNjb3BlLnJlc2VydmFzID0gW107XG4gICAgJHNjb3BlLnR1cm5vcyA9IFtdO1xuICAgIHZhciBkaWEgPSBudWxsO1xuICAgIHZhciBhbGdvID0gbnVsbDtcbiAgICAkc2NvcGUucmVzZXJ2YXNtID0gW107XG4gICAgJHNjb3BlLmNhbGVuZGFyaW8gPSBbXTtcbiAgICAvLyAtLS0tIEdFVCAtLS0tLVxuICAgICRzY29wZS5tZW51cyA9IFtdO1xuICAgIE1lbnVzRmFjdG9yeS5zdnIucXVlcnkoZnVuY3Rpb24obWVudXMpe1xuICAgICAgICAgICAgJHNjb3BlLm1lbnVzID0gbWVudXM7XG4gICAgfSk7XG5cbnZhciBOb2VqZWN1dGFkbyA9IHRydWU7XG4gICAgICAgIHZhciBzdG9wO1xuICAgICAgICAgIHN0b3AgPSAkaW50ZXJ2YWwoZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgIGlmIChOb2VqZWN1dGFkbyl7XG4gICAgICAgICAgICAgICAgTm9lamVjdXRhZG8gPSBmYWxzZTtcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICB2YXIgY2FsZW5kYXJpbyA9IFJlc2VydmFzRmFjdG9yeS5jYWxlbmRhcmlvO1xuICAgICAgICAgICAgICAgIERpc3BzRmFjdG9yeS5kaXNwb25pYmxlLnF1ZXJ5KGZ1bmN0aW9uKGRpc3BzKXtcbiAgICAgICAgICAgICAgICAgICAgQ29tcHJvdmFyRGlzcG9uaWJpbGlkYWQoZGlzcHMsY2FsZW5kYXJpbyk7XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfSwgNSk7XG4kc2NvcGUuZGlzcHMgPSBbXTtcbiAgICBEaXNwc0ZhY3Rvcnkuc3ZyLnF1ZXJ5KGZ1bmN0aW9uKGRpc3BzKXtcbiAgICAgICAgICAgICRzY29wZS5kaXNwcyA9IGRpc3BzO1xuICAgICAgICAgICAgdmFyIGYgPSBuZXcgRGF0ZSgpO1xuICAgICAgICAgICAgdmFyIGhveSA9IGYuZ2V0RGF5KCkgKyBcIi9cIiArIChmLmdldE1vbnRoKCkgKyAxKSArIFwiL1wiICsgZi5nZXRGdWxsWWVhcigpO1xuICAgICAgICAgICAgY29uc29sZS5sb2coaG95KTtcbiAgICAgICAgICAgIGRpc3BzLmZvckVhY2goZnVuY3Rpb24oZSxpKXtcbiAgICAgICAgICAgICAgICB2YXIgZCA9IG5ldyBEYXRlKGUuZGlhKTsgXG4gICAgICAgICAgICAgICAgdmFyIGRpYSA9IGQuZ2V0RGF0ZSgpICsgXCIvXCIgKyAoZC5nZXRNb250aCgpICsxKSArIFwiL1wiICsgZC5nZXRGdWxsWWVhcigpO1xuICAgICAgICAgICAgICAgIGlmIChmID4gZCl7XG4gICAgICAgICAgICAgICAgICAgIGlmIChob3kgIT0gZGlhKXtcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGRpYSk7XG4gICAgICAgICAgICAgICAgICAgICAgICBEaXNwc0ZhY3Rvcnkuc3ZyLmRlbGV0ZSh7aWQ6ZS5faWR9LGZ1bmN0aW9uKGwpe1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCRzY29wZS5kaXNwcy5pbmRleE9mKGUpKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkc2NvcGUuZGlzcHMuc3BsaWNlKCRzY29wZS5kaXNwcy5pbmRleE9mKGUpLDEpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSk7XG4gICAgICAgIFxuICAgIFJlc2VydmFzRmFjdG9yeS5zdnIucXVlcnkoZnVuY3Rpb24ocmVzZXJ2YXMpe1xuICAgICAgICAkc2NvcGUucmVzZXJ2YXMgPSByZXNlcnZhcztcbiAgICB9KTtcbiAgICAgICAgJHNjb3BlLiRvbignQ29nZXJEaWEnLCBmdW5jdGlvbigpe1xuICAgICAgICAgICAgJHNjb3BlLnJlc2VydmFzbSA9IFtdO1xuICAgICAgICAgICAgJHNjb3BlLkRpYSA9IFJlc2VydmFzRmFjdG9yeS5kaWE7XG4gICAgICAgICAgICAkc2NvcGUuZGlzcHMuZm9yRWFjaChmdW5jdGlvbihlLGkpe1xuICAgICAgICAgICAgICAgIGRpYSA9IG51bGw7XG4gICAgICAgICAgICAgICAgdmFyIG51ZXZvID0gbmV3IERhdGUoRGF0ZS5wYXJzZShlLmRpYSkpO1xuICAgICAgICAgICAgICAgIGRpYSA9IChudWV2by5nZXREYXRlKCkrXCIvXCIrKG51ZXZvLmdldE1vbnRoKCkrMSkrXCIvXCIrbnVldm8uZ2V0RnVsbFllYXIoKSk7XG4gICAgICAgICAgICAgICAgLy9jb25zb2xlLmxvZyhcIkxpc3RhOiBcIitkaWEpO1xuICAgICAgICAgICAgICAgIC8vY29uc29sZS5sb2coXCJFbnRyYWRvOiBcIiskc2NvcGUuRGlhKTtcbiAgICAgICAgICAgICAgICBpZiAoJHNjb3BlLkRpYSA9PSBkaWEpe1xuICAgICAgICAgICAgICAgICAgICAvL2NvbnNvbGUubG9nKGUuX2lkKTtcbiAgICAgICAgICAgICAgICAgICAgJHNjb3BlLnJlc2VydmFzLmZvckVhY2goZnVuY3Rpb24oYSxpKXtcbiAgICAgICAgICAgICAgICAgICAgaWYgKGUuX2lkID09IGEuZGlhKXtcbiAgICAgICAgICAgICAgICAgICAgICAgICRzY29wZS5yZXNlcnZhc20ucHVzaChhKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9KTtcbiAgICAkc2NvcGUuZWRpdGFyUmVzZXJ2YSA9IGZ1bmN0aW9uKHJlc2VydmEpIHtcbiAgICAgICAgUmVzZXJ2YXNGYWN0b3J5LmVkaXQgPSByZXNlcnZhO1xuICAgICAgICAkbG9jYXRpb24ucGF0aCgnL2VkaXRhclJlc2VydmEnKTtcbiAgICB9O1xuXG4gICAgJHNjb3BlLmVsaW1pbmFyUmVzZXJ2YSA9IGZ1bmN0aW9uKHJlc2VydmEpIHtcbiAgICAgICAgY29uc29sZS5sb2cocmVzZXJ2YS5faWQpO1xuICAgICAgICBjb25zb2xlLmxvZyhyZXNlcnZhLm51bXBlcnNvbmFzKTtcbiAgICAgICAgRGlzcHNGYWN0b3J5LnN2ci51cGRhdGUoe1wiX2lkXCI6cmVzZXJ2YS5kaWEsXCJ0dXJub19pZFwiOnJlc2VydmEudHVybm9faWQsXCJwbGF6YXNcIiA6IHJlc2VydmEubnVtcGVyc29uYXMsXCJhY2Npb25cIjpcIm1hc1wifSk7XG4gICAgICAgIFJlc2VydmFzRmFjdG9yeS5zdnIuZGVsZXRlKHtpZDpyZXNlcnZhLl9pZH0sZnVuY3Rpb24obCl7XG4gICAgICAgICAgICBjb25zb2xlLmxvZygkc2NvcGUucmVzZXJ2YXMuaW5kZXhPZihyZXNlcnZhKSk7XG4gICAgICAgICAgICAkc2NvcGUucmVzZXJ2YXMuc3BsaWNlKCRzY29wZS5yZXNlcnZhcy5pbmRleE9mKHJlc2VydmEpLDEpO1xuICAgICAgICAgICAgJHNjb3BlLnJlc2VydmFzbS5zcGxpY2UoJHNjb3BlLnJlc2VydmFzbS5pbmRleE9mKHJlc2VydmEpLDEpO1xuICAgICAgICB9KTtcbiAgICAgICAgY29uc29sZS5sb2cocmVzZXJ2YSk7XG4gICAgfTtcblxuICAgIGZ1bmN0aW9uIENvbXByb3ZhckRpc3BvbmliaWxpZGFkKGRpc3BvbmlibGVzLGNhbGVuZGFyaW8pe1xuICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IGNhbGVuZGFyaW8ubGVuZ3RoOyBpKyspeyAgICAgICAgXG4gICAgICAgICAgICB2YXIgZGlzcG9uaWJsZSA9IGZhbHNlO1xuICAgICAgICB2YXIgYyA9IGNhbGVuZGFyaW9baV07XG4gICAgICAgIHZhciBkaWFjID0gJChjKS50ZXh0KCkrIFwiL1wiK2dfY2FsZW5kYXJPYmplY3RbJ2N1cnJlbnRNb250aCddK1wiL1wiK2dfY2FsZW5kYXJPYmplY3RbJ2N1cnJlbnRZZWFyJ107XG4gICAgICAgIGRpc3BvbmlibGVzLmZvckVhY2goZnVuY3Rpb24oZSxpKXtcbiAgICAgICAgICAgIHZhciBudWV2byA9IG5ldyBEYXRlKERhdGUucGFyc2UoZS5kaWEpKTtcbiAgICAgICAgICAgIGRpYSA9IChudWV2by5nZXREYXRlKCkrXCIvXCIrKG51ZXZvLmdldE1vbnRoKCkrMSkrXCIvXCIrbnVldm8uZ2V0RnVsbFllYXIoKSk7XG4gICAgICAgICAgICBpZiAoZGlhID09IGRpYWMpIHtcbiAgICAgICAgICAgICAgICBkaXNwb25pYmxlID0gdHJ1ZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgICAgIGlmIChkaXNwb25pYmxlKXtcbiAgICAgICAgICAgICQoYykuYXR0cihcImRpc3BvbmlibGVcIix0cnVlKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICQoYykuYXR0cihcImRpc3BvbmlibGVcIixmYWxzZSk7XG4gICAgICAgICAgICAkKGMpLmNzcyhcImJhY2tncm91bmRcIixcInVybChodHRwczovL3Byb2plY3RvZmluYWwtcnJvZHJpZ3VlejIzLmM5LmlvL2ltZy9vcmFuZ2VfZGF5Tm9ybWFsLmdpZikgMCUgMCUgbm8tcmVwZWF0XCIpOyBcbiAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgICQoXCIjY2FsZW5kYXJpbGxpc3RhXCIpLmF0dHIoXCJhY3R1YWxpemFyXCIsZmFsc2UpO1xuICAgIH1cbiAgICBcbiAgJHNjb3BlLkNvbXByb3ZhckRpc3BvbmliaWxpZGFkID0gZnVuY3Rpb24oKXtcbiAgICAgICAgY29uc29sZS5sb2coXCJhbGdvXCIpOyAgIFxuICB9XG59KTsiLCIvLyAtLS0tIFJFU09VUkNFIEFOR1VMQVIgLS0tLS0tLVxuYW5ndWxhci5tb2R1bGUoJ1Jlc2VydmFzT25saW5lJykuZmFjdG9yeShcIlJlc2VydmFzRmFjdG9yeVwiLCBmdW5jdGlvbigkcmVzb3VyY2UsJHJvb3RTY29wZSkge1xuICAgIHJldHVybiB7XG4gICAgICAgIHN2ciA6ICRyZXNvdXJjZShcIi9hcGkvcmVzZXJ2YS86aWRcIiwgbnVsbCxcbiAgICAgICAge1xuICAgICAgICAgICAgJ3VwZGF0ZSc6IHsgbWV0aG9kOidQVVQnIH1cbiAgICAgICAgfSlcbiAgICAgICAgLFxuICAgICAgICBlZGl0IDogbnVsbCxcbiAgICAgICAgZGlhIDogbnVsbCxcbiAgICAgICAgQ29nZXJEaWEgOiBmdW5jdGlvbihkaWEpe1xuICAgICAgICAgICAgdGhpcy5kaWEgPSBkaWE7XG4gICAgICAgICAgICAkcm9vdFNjb3BlLiRicm9hZGNhc3QoJ0NvZ2VyRGlhJyk7XG4gICAgICAgICAgICAkcm9vdFNjb3BlLiRhcHBseSgpO1xuICAgICAgICB9LFxuICAgICAgICBjYWxlbmRhcmlvIDogbnVsbCxcbiAgICAgICAgQ29nZXJDYWxlbmRhcmlvIDogZnVuY3Rpb24oY2FsZW5kYXJpbyl7XG4gICAgICAgICAgICB0aGlzLmNhbGVuZGFyaW8gPSBjYWxlbmRhcmlvO1xuICAgICAgICB9XG4gICAgfVxufSk7Il0sInNvdXJjZVJvb3QiOiIvc291cmNlLyJ9