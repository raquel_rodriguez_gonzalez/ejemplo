var db = require("../db");
var Disponibilitat = db.model('Disponibilitat', {
   dia: {
       type: Date,
       required: true
    },
    turno: [{ 
        nombre : {
            type: String, 
            required: true
        },
        plazas : {
            type: Number, 
            required:true
        }
    }]
    
});
//Disponibilitat.methods.ComprobarPlazas = function(NumeroPlazas) {
//    return this.model('Disp').find({type:this.type},NumeroPlazas);
//}
module.exports = Disponibilitat;