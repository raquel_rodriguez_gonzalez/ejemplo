var db = require("../db");
var Menu = db.model('Menu', {
   nombre: {
       type: String,
       required: true
    },
    primeros: {
       type: String,
       required: true
    },
    segundos: {
       type: String,
       required: true
    },
    precio: {
        type: Number,
        required: true
    },
    imagen: {
        type: db.Schema.Types.ObjectId,
        ref: 'imagenesmenus',
        required: false
    }
});
module.exports = Menu;