var db = require("../db");
var Inicio = db.model('Inicio', {
   nombre: {
       type: String,
       required: true
    },
   informacion: {
       type: String,
       required: true
    }
});
module.exports = Inicio;