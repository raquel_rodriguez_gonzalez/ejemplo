var db = require("../db");

var schema = new db.Schema({
    originalName: {
        type: String,
        required: true
    },
    name:  {
        type: String,
        required: true
    },
    extension:  {
        type: String,
        required: true
    }
});

var imagenesmenus = db.model("imagenesmenus", schema);
module.exports = imagenesmenus;