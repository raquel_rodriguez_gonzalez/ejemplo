var db = require("../db");
var Reserva = db.model('Reserva', {
   nombre: {
       type: String,
       required: true
    },
    contacto: {
       type: String,
       required: true
    },
    numpersonas: {
        type: Number,
        required: true
    },
   menu: {
       type: db.Schema.Types.ObjectId,
       ref: 'Menu'
    },
   dia: {
       type: db.Schema.Types.ObjectId,
       ref: 'Disponibilitat'
    },
    turno:{
        type: String,
        required: true
    },
    turno_id:{
        type: String,
        required: true
    },
    importe: {
        type: Number,
        required: true
    }
});
module.exports = Reserva;