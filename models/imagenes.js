var db = require("../db");

var schema = new db.Schema({
    titol : {
        type : String,
        required : false
    },
    descripcio: {
        type : String,
        required: false
    },
    originalName: {
        type: String,
        required: true
    },
    name:  {
        type: String,
        required: true
    },
    extension:  {
        type: String,
        required: true
    }
});

var Imatge = db.model("Imatge", schema);
module.exports = Imatge;