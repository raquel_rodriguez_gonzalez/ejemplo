// ----- VARIABLES GLOBALS ----
var express = require("express");
var bodyParser = require("body-parser");
var multer = require("multer");

var app = express();
var http = require("http").Server(app);

var port = process.env.PORT || 90;
var ip = process.env.IP || '0.0.0.0';
app.use(bodyParser.json());

// ----- REFERENCIA A CONTROLADORS -----
app.use(require("./auth"));

app.use("/api/disp", require("./controllers/api/disponibilitat"));
app.use("/api/menu",multer( {dest : "./uploads/"}));
app.use("/api/menu", require("./controllers/api/menu"));
app.use("/api/inici", require("./controllers/api/inicio"));
app.use("/api/reserva", require("./controllers/api/reserva"));
app.use("/api/sessions", require("./controllers/api/sessions"));
app.use("/api/users", require("./controllers/api/users"));
app.use("/api/mailing", require("./controllers/api/mailing"));

app.use("/api/imatges",multer( {dest : "./uploads/"})); // uploads és la carpeta on aniran els fitxer quen es fa post
app.use("/api/imatges", require("./controllers/api/imagenes")); // Per carregar imatges farem servir mutter
app.use("/api/imagenesmenus", require("./controllers/api/imagenesmenus")); // Per carregar imatges farem servir mutter


// ------ REFERENCIA A ARXIUS ESTATICS ------
app.use("/", require("./controllers/static"));

//--------- LISTEN ----------
http.listen(port,ip,  function() {
	console.log('server listening on', port);
});
