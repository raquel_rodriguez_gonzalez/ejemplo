// ---- RESOURCE ANGULAR -------
angular.module('ReservasOnline').factory("MailingFactory", function($resource) {
    return {
        svr : $resource("/api/mailing/:id", null,
        {
            'update': { method:'PUT' }
        })
    }
    
});