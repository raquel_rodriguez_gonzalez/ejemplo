angular.module('ReservasOnline')
.filter("nl2br", function($filter) {
 return function(data) {
   if (!data) return data;
   return data.replace(/\n\r?/g, '<br />');
 };
})
    .controller("ApplicationController", function($interval,$scope,$location,UserSvc,$cookies,imatgesService,InicioFactory) {
    InicioFactory.svr.query(function(inicio){
        $scope.inicio = inicio;
    });
    
    $scope.ModificarInformacion = function(){
            InicioFactory.svr.update({"_id":$scope.inicio[0]._id,
                                        "nombre":  $scope.inicio[0].nombre,
                                        "informacion": $scope.inicio[0].informacion
            });        
    };
        $scope.$on('login', function(e,user) {
            $scope.currentUser = user;
        });
        $scope.logout = function(){
            UserSvc.logOut();
            delete $scope.currentUser;
            $location.path('/');
        };


    imatgesService.fetch()
        .success(function(imatges){
            $scope.imatges = imatges;
        });
        
        
    $scope.layoutDone = function() {
        $('.slider').slider();
    };
    

    });