// ---- RESOURCE ANGULAR -------
angular.module('ReservasOnline').factory("InicioFactory", function($resource) {
    return {
        svr : $resource("/api/inici/:id", null,
        {
            'update': { method:'PUT' }
        })
        ,
        edit : null
    }
    
});