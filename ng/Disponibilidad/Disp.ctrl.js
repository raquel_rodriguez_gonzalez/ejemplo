angular.module('ReservasOnline').controller('DispController',function($scope,$location,DispsFactory) {
    $scope.disps = [];

    // ---- GET -----

    DispsFactory.svr.query(function(disps){
        $scope.disps = disps;
        
    });

    $scope.editarDisp = function(disp) {
        DispsFactory.edit = disp;
        $location.path('/editardisp');
    };

    $scope.eliminarDisp = function(disp) {
        console.log(disp._id);
        DispsFactory.svr.delete({id:disp._id},function(l){
            console.log($scope.disps.indexOf(disp));
            $scope.disps.splice($scope.disps.indexOf(disp),1);
        });
        console.log(disp);
    };
});