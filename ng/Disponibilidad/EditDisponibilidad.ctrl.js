angular.module('ReservasOnline').controller('EditarDispController',function($scope,$location,DispsFactory) {
            $scope.editDia = DispsFactory.edit.dia;
            $scope.editPlazas = DispsFactory.edit.plazas;

$scope.updateDisp = function() {
        if ($scope.editDia && $scope.editPlazas) {
            DispsFactory.svr.update({"_id":DispsFactory.edit._id},
                function() {
                $location.path('/');
            });
        }
    };
});