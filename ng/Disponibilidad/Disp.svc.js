// ---- RESOURCE ANGULAR -------
angular.module('ReservasOnline').factory("DispsFactory", function($resource) {
    return {
        svr : $resource("/api/disp/:id", null,
        {
            'update': { method:'PUT' }
        }),
        disponible : $resource("/api/disp/disponibles", null),
        edit : null,
        
    }
    
});