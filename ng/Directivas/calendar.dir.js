angular.module('ReservasOnline')
 .directive('calendarinou', function(ReservasFactory) {
        return {
          restrict: 'E',
          template: '<div class="col-md-6" style="height:200px;"id="calendarnou"></div>',
          link: function (scope, element) {
                g_calendarObject = new JsDatePick({
                        useMode:1,
                        isStripped:true,
                        target:"calendarnou",
                	  cellColorScheme:"aqua"
                    });
                    g_calendarObject.setOnSelectedDelegate(function(){
                        var obj = g_calendarObject.getSelectedDay();
                        ReservasFactory.CogerDia(obj.day + "/" + obj.month + "/" + obj.year);
                        //alert("a date was just selected and the date is : " + obj.day + "/" + obj.month + "/" + obj.year);
                    });
                    ReservasFactory.CogerCalendario($("div[class^='day']")); 
                  }
            };
      });