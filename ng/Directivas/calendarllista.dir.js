angular.module('ReservasOnline')
 .directive('calendarillista', function(ReservasFactory) {
        return {
          restrict: 'E',
          template: '<div class="col-md-6" style="height:200px;"id="calendarillista"></div>',
          link: function (scope, element) {
                g_calendarObject = new JsDatePick({
                        useMode:1,
                        isStripped:true,
                        target:"calendarillista",
                	  cellColorScheme:"armygreen"
                    });
                g_calendarObject.setOnSelectedDelegate(function(){
                        var obj = g_calendarObject.getSelectedDay();
                        ReservasFactory.CogerDia(obj.day + "/" + obj.month + "/" + obj.year);
                    });
                    ReservasFactory.CogerCalendario($("div[class^='day']"));                    
                    
                  }
            };
      });