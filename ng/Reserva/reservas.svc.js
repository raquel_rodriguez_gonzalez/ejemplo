// ---- RESOURCE ANGULAR -------
angular.module('ReservasOnline').factory("ReservasFactory", function($resource,$rootScope) {
    return {
        svr : $resource("/api/reserva/:id", null,
        {
            'update': { method:'PUT' }
        })
        ,
        edit : null,
        dia : null,
        CogerDia : function(dia){
            this.dia = dia;
            $rootScope.$broadcast('CogerDia');
            $rootScope.$apply();
        },
        calendario : null,
        CogerCalendario : function(calendario){
            this.calendario = calendario;
        }
    }
});