angular.module('ReservasOnline').controller('EditarReservaController',function($scope,$interval,$location,ReservasFactory, MenusFactory, DispsFactory) {
$scope.editNombre = ReservasFactory.edit.nombre;
$scope.editContacto = ReservasFactory.edit.contacto;
$scope.editNumPersonas = ReservasFactory.edit.numpersonas;
$scope.editImporte = ReservasFactory.edit.importe;
$scope.menus = [];
$scope.disps = [];
$scope.turnos = [];
var Antdia = ReservasFactory.edit.dia;
var AntTurno_id = ReservasFactory.edit.turno_id;
var AntNumPersonas = ReservasFactory.edit.numpersonas;
var dia = null;
var entrado = null;
var turno = null;
var algo = true;
        var stop;
          stop = $interval(function() {
              if (algo){
                  algo = false;
                $('select').material_select();
              }
          }, 500);



        MenusFactory.svr.query(function(menus){
            $scope.menus = menus;
        });
        DispsFactory.svr.query(function(disps){
            $scope.disps = disps;
        });
        $scope.$watch('editMenu', function(newVal) {
            if ($scope.editMenu && $scope.editNumPersonas){
            $scope.menus.forEach(function(e,i){
                if (e._id == $scope.editMenu._id) {
                    $scope.editImporte = e.precio * $scope.editNumPersonas;
                }    
            });
            } else {
               $scope.editImporte = 0; 
            }
        });
        $scope.$on('CogerDia', function(){
            entrado = ReservasFactory.dia;
            $scope.disps.forEach(function(e,i){
                var nuevo = new Date(Date.parse(e.dia));
                dia = (nuevo.getDate()+"/"+(nuevo.getMonth()+1)+"/"+nuevo.getFullYear());
                if (dia == entrado){
                    $scope.turnos = e.turno;
                    $scope.editDia = e._id;
                    $scope.$apply();
                    $('select').material_select();
                }
            });
        });
        $scope.$watch('editTurno', function(newVal) {
            $scope.turnos.forEach(function(e,i){
                if (e._id == $scope.editTurno._id){
                    turno = e.nombre;
                    $scope.plazas = e.plazas;
                    if($scope.plazas < $scope.editNumPersonas) {
                        $scope.disponible = true;
                    } else {
                        $scope.disponible = false;
                    }
                }
            });
        });


$scope.updateReserva = function() {
        if ($scope.editNombre && $scope.editContacto && $scope.editNumPersonas
        && $scope.editMenu) {
            ReservasFactory.svr.update({"_id":ReservasFactory.edit._id,
                                        "nombre":  $scope.editNombre,
                                        "contacto": $scope.editContacto,
                                        "numpersonas": $scope.editNumPersonas,
                                        "menu": $scope.editMenu._id,
                                        "dia" : $scope.editDia,
                                        "turno" : turno,
                                        "turno_id" : $scope.editTurno._id,
                                        "importe" : $scope.editImporte
            },
                function() {
                    DispsFactory.svr.update({"_id":Antdia,"turno_id":AntTurno_id,"plazas" : AntNumPersonas,"accion" : "mas"});
                    DispsFactory.svr.update({"_id":$scope.editDia,"turno_id":$scope.editTurno._id,"plazas" : $scope.editNumPersonas,"accion" : "menos"});
                $location.path('/reservas');
            });
        }
    };
});