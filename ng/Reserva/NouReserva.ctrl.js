angular.module('ReservasOnline').controller('NouReservaController',function(imatgesServiceMenus,$rootScope,$interval,$scope,ReservasFactory,$location, MenusFactory,DispsFactory, MailingFactory) {
$scope.menus = [];
$scope.disps = [];
$scope.turnos = [];
$scope.plazas;
$scope.disponible = false;
$scope.Importe = 0;
$scope.mostrar = false;
var codigoValidar = "1234";

var Noejecutado = true;
        var stop;
          stop = $interval(function() {
              if (Noejecutado){
                  Noejecutado = false;
                $('select').material_select();
                var calendario = ReservasFactory.calendario;
                DispsFactory.disponible.query(function(disps){
                    ComprovarDisponibilidad(disps,calendario);
                });
              }
          }, 500);
var dia = null;
var entrado = null;
var turno = null;
    imatgesServiceMenus.fetch()
        .success(function(imatges){
            $scope.imatges = imatges;
    });
        MenusFactory.svr.query(function(menus){
            $scope.menus = menus;
        });
        DispsFactory.disponible.query(function(disps){
            $scope.disps = disps;
        });
        $scope.$watch('Menu', function(newVal) {
            if ($scope.Menu && $scope.NumPersonas){
            $scope.menus.forEach(function(e,i){
                if (e._id == $scope.Menu._id) {
                    $scope.Importe = e.precio * $scope.NumPersonas;
                }    
            });
            } else {
               $scope.Importe = 0; 
            }
        });
        $scope.$on('CogerDia', function(){
            entrado = ReservasFactory.dia;
            $scope.encontrado = false;
            $scope.disps.forEach(function(e,i){
                var nuevo = new Date(Date.parse(e.dia));
                dia = (nuevo.getDate()+"/"+(nuevo.getMonth()+1)+"/"+nuevo.getFullYear());
                if (dia == entrado){
                    $scope.Turno = null;
                    $scope.turnos = e.turno;
                    $scope.Dia = e._id;
                    $scope.encontrado = true;
                    $scope.mostrar = true;
                    console.log("mostrar: "+$scope.mostrar);
                    $scope.$apply();
                    $('select').material_select();
                }
            });
            if (!$scope.encontrado){
                $scope.turnos = [];
                $scope.mostrar = false;
            }
        });
        var correcto;
        $scope.$watch('NumPersonas',function(newVal)   {
            if (!isNaN($scope.NumPersonas)){
                $scope.correcto = true;
            } else {
                $scope.correcto = false;
            }
        })
        $scope.$watch('Turno', function(newVal) {
            $scope.turnos.forEach(function(e,i){
                if (e._id == $scope.Turno._id){
                    turno = e.nombre;
                    $scope.plazas = e.plazas;
                    if($scope.plazas < $scope.NumPersonas) {
                        $scope.disponible = true;
                    } else {
                        $scope.disponible = false;
                    }
                }
            });
        });

        $scope.afegirReserva = function() {
            console.log (codigoValidar == $scope.Codigo);
            if (codigoValidar == $scope.Codigo || $scope.currentUser){
                

        if ($scope.Nombre && $scope.Contacto && $scope.correcto
        && $scope.Menu && $scope.Dia ) {
            ReservasFactory.svr.save({
                nombre : $scope.Nombre,
                contacto: $scope.Contacto,
                numpersonas : $scope.NumPersonas,
                menu : $scope.Menu._id,
                dia : $scope.Dia,
                turno : turno,
                turno_id : $scope.Turno._id,
                importe: $scope.Importe,
            }, function(reserva) {
                DispsFactory.svr.update({"_id":$scope.Dia,"turno_id":$scope.Turno._id,"plazas" : $scope.NumPersonas,"accion" : "menos"});
                $scope.Nombre = null;
                $scope.Contacto = null;
                $scope.NumPersonas = null;
                $scope.Menu = null;
                $scope.Dia = null;
                turno = null;
                $scope.Turno = null;
                $scope.Importe = null;
                if ($scope.currentUser) {
                    $location.path('/reservas');
                } else {
                    $location.path('/');
                }

            });
        } else {  console.log("DATOS INCORRECTO O INCOMPLETOS")
            }
    } else {
                console.log("CODIGO INCORRECTO");
            }
    };
    
    function ComprovarDisponibilidad(disponibles,calendario){
        for (var i = 0; i < calendario.length; i++){        
            var disponible = false;
        var c = calendario[i];
        var diac = $(c).text()+ "/"+g_calendarObject['currentMonth']+"/"+g_calendarObject['currentYear'];
        disponibles.forEach(function(e,i){
            var nuevo = new Date(Date.parse(e.dia));
            dia = (nuevo.getDate()+"/"+(nuevo.getMonth()+1)+"/"+nuevo.getFullYear());
            if (dia == diac) {
                disponible = true;
            }
        });
        if (disponible){
            $(c).attr("disponible",true);
        } else {
            $(c).attr("disponible",false);
            $(c).css("background","url(https://projectofinal-rrodriguez23.c9.io/img/orange_dayNormal.gif) 0% 0% no-repeat"); 
        }
        //console.log(disponible);
        }
    }
    
    
    $scope.EnviarCodigo = function() {
        codigoValidar = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) { 
            var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8); return v.toString(16);
        });

        MailingFactory.svr.save({
            "email" : $scope.Email,
            "codigo" : codigoValidar
        });
    }
});