// ---- RESOURCE ANGULAR -------
angular.module('ReservasOnline').factory("MenusFactory", function($resource) {
    return {
        svr : $resource("/api/menu/:id", null,
        {
            'update': { method:'PUT' }
        })
        ,
        edit : null
    }
    
});