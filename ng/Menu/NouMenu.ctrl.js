angular.module('ReservasOnline').controller('NouMenuController',function($scope,MenusFactory, $location,FileUploader) {
    var uploader = $scope.uploader = new FileUploader({url:"/api/menu",alias:"foto",removeAfterUpload: true});
        uploader.onBeforeUploadItem = function (item) {
            item.formData.push({titol: ""});
            item.formData.push({descripcion: ""});
            item.formData.push({nombre: $scope.nombre});
            item.formData.push({primeros: $scope.primeros});
            item.formData.push({segundos: $scope.segundos});
            item.formData.push({precio: $scope.precio});
        };
        
        $scope.afegirMenu = function() {
        if ($scope.Nombre && $scope.Primeros && $scope.Segundos 
        && $scope.Precio) {
            MenusFactory.svr.save({
                nombre : $scope.Nombre,
                primeros: $scope.Primeros,
                segundos: $scope.Segundos,
                precio: $scope.Precio,
                foto : $scope.foto
            }, function(autor) {
                $scope.Nombre = null;
                $scope.Primeros = null;
                $scope.Segundos = null;
                $scope.Precio = null;
                $scope.foto = null;
                $location.path('/menus');
            });
        }
    };
});