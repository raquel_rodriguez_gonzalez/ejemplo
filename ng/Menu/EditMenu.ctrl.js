angular.module('ReservasOnline').controller('EditarMenuController',function($scope,$location,MenusFactory) {
            $scope.editNombre = MenusFactory.edit.nombre;
            $scope.editPrimeros = MenusFactory.edit.primeros;
            $scope.editSegundos = MenusFactory.edit.segundos;
            $scope.editPrecio = MenusFactory.edit.precio;

$scope.updateMenu = function() {
        if ($scope.editNombre && $scope.editPrimeros && $scope.editSegundos 
        && $scope.editPrecio) {
            MenusFactory.svr.update({"_id":MenusFactory.edit._id,
                                        "nombre":  $scope.editNombre,
                                        "primeros": $scope.editPrimeros,
                                        "segundos": $scope.editSegundos,
                                        "precio": $scope.editPrecio
            },
                function() {
                $location.path('/menus');
            });
        }
    };
});