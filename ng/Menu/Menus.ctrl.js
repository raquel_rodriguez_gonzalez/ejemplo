angular.module('ReservasOnline').controller('MenusController',function($scope,$location,MenusFactory,imatgesServiceMenus) {
    $scope.menus = [];

    // ---- GET -----
    imatgesServiceMenus.fetch()
        .success(function(imatges){
            $scope.imatges = imatges;
    });


    MenusFactory.svr.query(function(menus){
        $scope.menus = menus;
    });

    $scope.editarMenu = function(menu) {
        MenusFactory.edit = menu;
        $location.path('/editarmenu');
    };

    $scope.eliminarMenu = function(menu) {
        console.log(menu._id);
        MenusFactory.svr.delete({id:menu._id},function(l){
            console.log($scope.menus.indexOf(menu));
            $scope.menus.splice($scope.menus.indexOf(menu),1);
        });
        console.log(menu);
    };
});