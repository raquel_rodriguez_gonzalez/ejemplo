angular.module('ReservasOnline')
    .config(function($routeProvider, $locationProvider) {
        $routeProvider
            .when("/", {
                controller: 'ApplicationController',
                templateUrl: '/inicio.html',
                autoritzat: false
            })
            .when("/reservas", {
                controller: 'ReservasController',
                templateUrl: '/Reserva/reservas.html',
                autoritzat: true
            })
            .when("/administracion", {
                controller: 'ApplicationController',
                templateUrl: '/administracion.html',
                autoritzat: false
            })
            .when("/dias", {
                controller: 'DispController',
                templateUrl: '/Disponibilidad/disponibilitat.html',
                autoritzat: false
            })
            .when("/noudia", {
                controller: "NouDispController",
                templateUrl: '/Disponibilidad/nouDisp.html',
                autoritzat: false
            })
            .when("/editardisp", {
                controller: "EditarDispController",
                templateUrl: '/Disponibilidad/editarDisp.html',
                autoritzat: false
            })
            .when("/menus", {
                controller: 'MenusController',
                templateUrl: '/Menu/menus.html',
                autoritzat: false
            })
            .when("/noumenu", {
                controller: "NouMenuController",
                templateUrl: '/Menu/nouMenu.html',
                autoritzat: false
            })
            .when("/editarmenu", {
                controller: "EditarMenuController",
                templateUrl: '/Menu/editarMenu.html',
                autoritzat: true
            })
            .when("/noureserva", {
                controller: "NouReservaController",
                templateUrl: '/Reserva/nouReserva.html',
                autoritzat: false
            })
            .when("/editarReserva", {
                controller: "EditarReservaController",
                templateUrl: '/Reserva/editarReserva.html',
                autoritzat: true
            })
            .when("/login", {
                controller: "LoginController",
                templateUrl: "/login/login.html",
                autoritzat: false
            })
            .when("/administracion", {
                controller: 'mostrarImatgesCtrl',
                templateUrl: '/Imagenes/listaImagenes.html',
                autoritzat: true
            })
            .when("/cargarimagenes", {
                controller: 'SubirImatgeCtrl',
                templateUrl: '/Imagenes/cargarImagenes.html',
                autoritzat: true
            })
            .otherwise({
                redirectTo: '/'
            });
            
            $locationProvider.html5Mode({
                enabled: true,
                requireBase: false
            });
    })
    .run(function($rootScope,UserSvc) {
        $rootScope.$on('$routeChangeStart', function(event, next) {
           if (next)
                if (!UserSvc.auth & next.autoritzat) 
                    event.preventDefault();
        });
    });