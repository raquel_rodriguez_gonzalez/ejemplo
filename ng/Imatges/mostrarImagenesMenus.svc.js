angular.module('ReservasOnline')
    .service("imatgesServiceMenus", function($http) {
        this.fetch = function() {
            return $http.get("/api/imagenesmenus");
        };
        
        this.delete = function(imatge) {
            console.log(imatge._id);
            return $http.delete("/api/imagenesmenus/"+imatge._id);
        };
    });