angular.module('ReservasOnline')
    .service("imatgesService", function($http) {
        this.fetch = function() {
            return $http.get("/api/imatges");
        };
        
        this.delete = function(imatge) {
            console.log(imatge._id);
            return $http.delete("/api/imatges/"+imatge._id);
        };
    });