angular.module('ReservasOnline').controller("mostrarImatgesCtrl",function($scope,imatgesService) {
    imatgesService.fetch()
        .success(function(imatges){
            $scope.imatgesmenus = imatges;
        });
        
        
        $scope.eliminarImatge = function (imatge) {
            imatgesService.delete(imatge);
            $scope.imatges.splice($scope.imatges.indexOf(imatge),1);
        } 
        
});